package board;

import engine.Player;
import engine.PlayerBlack;
import engine.PlayerWhite;

/**
 * @author Daniel Samojlik & Elaine Yang Alliance is an enum type is used for
 *         players and the piece color. It is used to keep it type safe and
 *         allows for defined behavior.
 */
public enum Alliance {
	WHITE {
		@Override
		public int getDirection() {
			return -1;
		}

		@Override
		public boolean isWhite() {
			return true;
		}

		@Override
		public boolean isBlack() {
			return false;
		}

		@Override
		public Player choosePlayer(final PlayerWhite playerWhite, final PlayerBlack playerBlack) {
			return playerWhite;
		}

		@Override
		public boolean isPawnPromotionSquare(int coordinate) {
			return BoardMethods.First_Row[coordinate];
		}
	},
	BLACK {
		@Override
		public int getDirection() {
			return -1;
		}

		@Override
		public boolean isWhite() {
			return false;
		}

		@Override
		public boolean isBlack() {
			return true;
		}

		@Override
		public Player choosePlayer(final PlayerWhite playerWhite, final PlayerBlack playerBlack) {
			return playerBlack;
		}

		@Override
		public boolean isPawnPromotionSquare(int coordinate) {
			return BoardMethods.Eighth_Row[coordinate];
		}
	};

	/**
	 * This abstract method will return the integer affiliated direction in which
	 * pieces move. White moves from 63 to 0, and black moves from 0 to 63. Thus,
	 * the method returns -1 and 1, respectively.
	 * 
	 * @return int represents direction of piece movement
	 */
	public abstract int getDirection();

	/**
	 * This method returns true if the piece is white and false if it is black.
	 * 
	 * @return boolean
	 */
	public abstract boolean isWhite();

	/**
	 * This method returns true if the piece is black and false if it is white.
	 * 
	 * @return boolean
	 */

	public abstract boolean isBlack();
	
	/**
	 * This method checks if the row is a pawn promotion row.
	 * @param coordinate
	 * @return boolean
	 */
	
	public abstract boolean isPawnPromotionSquare(int coordinate);
	
	/**
	 * This abstract method will assign each player to their respective color. 
	 * @param playerWhite
	 * @param playerBlack
	 * @return Player
	 */

	public abstract Player choosePlayer(PlayerWhite playerWhite, PlayerBlack playerBlack);
}
