package board;

import pieces.Piece;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Daniel Samojlik & Elaine Yang
 *         <p>
 *         Below is the Square class, which creates the coordinates for the
 *         chess board. The class is designed to be immutable so that it cannot
 *         be changed externally. The class is abstract, where the two abstract
 *         methods include isSquareOccupied (checks if the square is empty) and
 *         getPiece (gets the piece in the square).
 */

public abstract class Square {
	/**
	 * This coordinate field will be used to denote each square on the chess board.
	 */
	protected final int coordinate;

	/**
	 * This variable Empty_Squares will be used to create a map of an empty board.
	 */

	private static final Map<Integer, EmptySquare> Empty_Squares = createAllPossibleEmptySquares();

	/**
	 * This method create the hash map of empty squares.
	 * 
	 * @return an immutable copy of the empty square map which was created.
	 */

	private static Map<Integer, EmptySquare> createAllPossibleEmptySquares() {

		/**
		 * The emptySquareMap instance will be used to obtain empty squares.
		 */

		final Map<Integer, EmptySquare> emptySquareMap = new HashMap<>();

		for (int i = 0; i < BoardMethods.Total_Squares; i++) {
			emptySquareMap.put(i, new EmptySquare(i));
		}

		return Collections.unmodifiableMap(emptySquareMap);
	}

	/**
	 * This is the only method that allows for a square on the board to be created.
	 * If the user wants to create an empty tile, the user will get a cached empty
	 * tile.
	 * 
	 * @param coordinate
	 * @param piece
	 * @return Square
	 */

	public static Square createSquare(final int coordinate, final Piece piece) {
		return piece != null ? new OccupiedSquare(coordinate, piece) : Empty_Squares.get(coordinate);
	}

	/**
	 * Here is the constructor for coordinate.
	 * 
	 * @param coordinate
	 */

	private Square(final int coordinate) {
		this.coordinate = coordinate;
	}

	/**
	 * Here is the abstract method to check if the square is occupied.
	 * 
	 * @return boolean
	 */

	public abstract boolean isSquareOccupied();

	/**
	 * Here is the abstract method to get the piece on the square.
	 * 
	 * @return Piece
	 */

	public abstract Piece getPiece();
	
	/**
	 * This method returns the coordinate associated with a square.
	 * @return int 
	 */
	
	public int getSquareCoordinate() {
		return this.coordinate;
	}

	/**
	 * EmptySquare is a subclass of Square and implements the two abstract methods.
	 * The isSquareOccupied method will return false because the square is empty.
	 * The getPiece method will return null because the square is empty. The
	 * toString method will print the two hash marks for the respective empty
	 * squares.
	 */

	public static final class EmptySquare extends Square {

		/**
		 * Constructor for coordinate of the empty square.
		 * 
		 * @param coordinate
		 */

		private EmptySquare(final int coordinate) {
			super(coordinate);
		}

		@Override
		public String toString() {
			int num = coordinate;
			if ((num < 8) || (num > 15 && num < 24) || (num > 31 && num < 40) || (num > 47 && num < 56)) {
				if (num % 2 != 0) {
					return "##";
				} else {
					return "";
				}
			} else {
				if (num % 2 == 0) {
					return "##";
				}
				else {
					return "";
				}
			}
		}		

		@Override
		public boolean isSquareOccupied() {
			return false;
		}

		@Override
		public Piece getPiece() {
			return null;
		}
	}

	/**
	 * OccupiedSquare is a subclass of Square and implements the two abstract
	 * methods. The isSquareOccupied method will return true because this square is
	 * an occupied square. This getPiece method will return the piece that is on the
	 * square. The toString method will print the piece on the given square.
	 */

	public static final class OccupiedSquare extends Square {

		/**
		 * This variable will be used to get the piece on the square.
		 */
		private final Piece pieceOnSquare;

		/**
		 * Here are the constructors for coordinate and pieceOnSquare.
		 * 
		 * @param coordinate
		 * @param pieceOnSquare
		 */

		private OccupiedSquare(int coordinate, final Piece pieceOnSquare) {
			super(coordinate);
			this.pieceOnSquare = pieceOnSquare;
		}

		@Override
		public String toString() {
			return getPiece().toString();
		}

		@Override
		public boolean isSquareOccupied() {
			return true;
		}

		@Override
		public Piece getPiece() {
			return this.pieceOnSquare;
		}
	}
}
