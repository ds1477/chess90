package board;

import java.util.Collection;

import board.Board.Builder;
import pieces.King;
import pieces.Pawn;
import pieces.Piece;
import pieces.Piece.PieceType;
import pieces.Rook;

/**
 * @author Daniel Samojlik & Elaine Yang
 *         <p>
 *         This Move class will be used to define and execute moves. The
 *         behavior of executing a move is based on creating and returning a new
 *         board based on the executing move for an incoming board.
 *
 */
public abstract class Move {

	/**
	 * Here are the fields for the Move class constructor.
	 */
	final Board board;
	final Piece movedPiece;
	final int destinationCoordinate;
	final boolean isFirstMove;

	/**
	 * Here is the constructor for the move class.
	 * 
	 * @param board
	 * @param movedPiece
	 * @param destinationCoordinate
	 */
	private Move(final Board board, final Piece movedPiece, final int destinationCoordinate) {
		this.board = board;
		this.movedPiece = movedPiece;
		this.destinationCoordinate = destinationCoordinate;
		this.isFirstMove = movedPiece.isFirstMove();
	}
	
	/**
	 * Here is the constructor for the move class, setting the isFirstMove to false.
	 * 
	 * @param board
	 * @param destinationCoordinate
	 */
	
	private Move (final Board board, final int destinationCoordinate) {
		this.board = board;
		this.destinationCoordinate = destinationCoordinate;
		this.movedPiece = null;
		this.isFirstMove = false;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + this.destinationCoordinate;
		result = prime * result + this.movedPiece.hashCode();
		result = prime * result + this.movedPiece.getPiecePosition();
		result = result + (isFirstMove ? 1 : 0);
		return result;
	}

	@Override
	public boolean equals(final Object other) {
		if (this == other) {
			return true;
		}

		if (!(other instanceof Move)) {
			return false;
		}
		final Move otherMove = (Move) other;
		return getCurrentCoordinate() == otherMove.getCurrentCoordinate() &&
				getDestinationCoordinate() == otherMove.getDestinationCoordinate()
				&& getMovedPiece().equals(otherMove.getMovedPiece()) && isFirstMove == otherMove.isFirstMove;
	}
	
	/**
	 * This method returns the current board.
	 * 
	 * @return board
	 */
	
	public Board getBoard() {
		return this.board;
	}

	/**
	 * This method returns the destination coordinate of the prospective move.
	 * 
	 * @return int coordinate
	 */
	public int getDestinationCoordinate() {
		return this.destinationCoordinate;
	}

	/**
	 * This method gets the moved piece.
	 * 
	 * @return Piece
	 */

	public Piece getMovedPiece() {
		return this.movedPiece;
	}

	/**
	 * This method returns the coordinate of the moved piece.
	 * 
	 * @return int
	 */
	public int getCurrentCoordinate() {
		return this.movedPiece.getPiecePosition();
	}

	/**
	 * This convenience method checks if the move is an attacking move.
	 * 
	 * @return boolean
	 */
	public boolean isAttack() {
		return false;
	}

	/**
	 * This convenience method checks if the move is a castling move.
	 * 
	 * @return boolean
	 */
	public boolean isCastlingMove() {
		return false;
	}

	/**
	 * This convenience method checks if the piece is getting attacked.
	 * 
	 * @return Piece
	 */
	public Piece getAttackedPiece() {
		return null;
	}

	/**
	 * This method will create a new board by set the pieces that have not been
	 * moved, then setting the moved piece and setting the next move to the other
	 * player.
	 * 
	 * @return Board the incoming board of the next move
	 */

	public Board execute() {

		//System.out.println("Entered Execute");
		final Builder builder = new Builder();
		final Builder tempBuilder = new Builder();
		this.movedPiece.empass = false;
		for (final Piece piece : this.board.currentPlayer().getActivePieces()) {
			if (!this.movedPiece.equals(piece)) {
				piece.empass = false;
				builder.setPiece(piece);
				tempBuilder.setPiece(piece);
			}
		}

		for (final Piece piece : this.board.currentPlayer().getOpponent().getActivePieces()) {
			piece.empass = false;
			builder.setPiece(piece);
			tempBuilder.setPiece(piece);
		}
		builder.setPiece(this.movedPiece.movePiece(this));
		//System.out.println("Half way");
		Piece tpiece;
		Alliance side = this.board.currentPlayer().getAlliance();
		if ((side == Alliance.BLACK && !board.blackCheck) || (side == Alliance.WHITE && !board.whiteCheck)) {
			tempBuilder.setPiece(this.movedPiece.movePiece(this));
			tempBuilder.setNextMove(this.board.currentPlayer().getOpponent().getAlliance());
			Boolean badmove = false;
			try {
				Board tempBoard = tempBuilder.build();
				Collection<Move> moveList = tempBoard.getAllLegalMoves(tempBoard.currentPlayer().getAlliance().isBlack());
				for (Move mv : moveList) {
					if (tempBoard.getSquare(mv.getDestinationCoordinate()).isSquareOccupied()) {
						tpiece = tempBoard.getSquare(mv.getDestinationCoordinate()).getPiece();
						if (tpiece.getPieceType().equals(PieceType.KING) && tpiece.getPieceAlliance() == side) {
							badmove = true;
							throw new java.lang.RuntimeException();
						}
					}
				}
			} catch (java.lang.RuntimeException e) {
				if (badmove) {
					throw new java.lang.RuntimeException();
				} else {
					//System.out.println(e);
					throw new java.lang.UnsupportedOperationException();
				}
			}
			
		}
		
		//System.out.println("Made it here ok");
		builder.setNextMove(this.board.currentPlayer().getOpponent().getAlliance());
		try {
			return builder.build();
		} catch (java.lang.RuntimeException e) {
			throw new java.lang.UnsupportedOperationException();
		}
		
	}

	/**
	 * This subclass extends Move and classifies moves that do not attack the
	 * opponent's piece.
	 */

	public static final class NonAttackingMove extends Move {

		/**
		 * Here is the constructor for the NonAttackingMove class.
		 * 
		 * @param board
		 * @param movedPiece
		 * @param destinationCoordinate
		 */
		public NonAttackingMove(final Board board, final Piece movedPiece, final int destinationCoordinate) {
			super(board, movedPiece, destinationCoordinate);
		}

		@Override
		public boolean equals(final Object other) {
			return this == other || other instanceof NonAttackingMove && super.equals(other);
		}

		@Override
		public String toString() {
			return movedPiece.getPieceType().toString()
					+ BoardMethods.getPositionAtCoordinate(this.destinationCoordinate);
		}
	}

	/**
	 * This subclass extends the Move class and classifies moves that attack
	 * opponent's pieces.
	 */

	public static class AttackingMove extends Move {
		/**
		 * This field represents the piece that is getting attacked.
		 */
		final Piece opposingPiece;

		/**
		 * Here is the constructor for the AttackingMove class.
		 * 
		 * @param board
		 * @param movedPiece
		 * @param destinationCoordinate
		 * @param opposingPiece
		 */
		public AttackingMove(final Board board, final Piece movedPiece, final int destinationCoordinate,
				final Piece opposingPiece) {
			super(board, movedPiece, destinationCoordinate);
			this.opposingPiece = opposingPiece;
		}

		@Override
		public int hashCode() {
			return this.opposingPiece.hashCode() + super.hashCode();
		}

		@Override
		public boolean equals(final Object other) {
			if (this == other) {
				return true;
			}

			if (!(other instanceof AttackingMove)) {
				return false;
			}
			final AttackingMove otherAttackingMove = (AttackingMove) other;
			return super.equals(otherAttackingMove) && getAttackedPiece().equals(otherAttackingMove.getAttackedPiece());
		}

		@Override
		public String toString() {
			return movedPiece.getPieceType().toString()
					+ BoardMethods.getPositionAtCoordinate(this.destinationCoordinate);
		}

		@Override
		public boolean isAttack() {
			return true;
		}

		@Override
		public Piece getAttackedPiece() {
			return this.opposingPiece;
		}
	}

	/**
	 * This subclass extends Move will represent the PawnMove which identifies non
	 * attacking pawn moves.
	 */
	public static final class PawnMove extends Move {

		/**
		 * Here is the constructor for the PawnMove class.
		 * 
		 * @param board
		 * @param movedPiece
		 * @param destinationCoordinate
		 */
		public PawnMove(final Board board, final Piece movedPiece, final int destinationCoordinate) {
			super(board, movedPiece, destinationCoordinate);
		}

		@Override
		public boolean equals(final Object other) {
			return this == other || other instanceof PawnMove && super.equals(other);
		}

		@Override
		public String toString() {
			return movedPiece.getPieceType().toString()
					+ BoardMethods.getPositionAtCoordinate(this.destinationCoordinate);
		}
	}

	/**
	 * This subclass extends AttackingMove and represents the PawnAttackMove which
	 * identifies attacking pawn moves.
	 */
	public static class PawnAttackMove extends AttackingMove {

		/**
		 * Here is the constructor for the PawnAttackMove class.
		 * 
		 * @param board
		 * @param movedPiece
		 * @param destinationCoordinate
		 * @param opposingPiece
		 */
		public PawnAttackMove(Board board, Piece movedPiece, int destinationCoordinate, Piece opposingPiece) {
			super(board, movedPiece, destinationCoordinate, opposingPiece);
		}

		@Override
		public boolean equals(final Object other) {
			return this == other || other instanceof PawnAttackMove && super.equals(other);
		}

		@Override
		public String toString() {
			return movedPiece.getPieceType().toString()
					+ BoardMethods.getPositionAtCoordinate(this.destinationCoordinate);
		}
	}

	/**
	 * This subclass extends AttackingMove and represents the
	 * EnPassantPawnAttackMove which identifies en passant attacking pawn moves.
	 */

	public static class EnPassantPawnAttackMove extends PawnAttackMove {

		/**
		 * Here is the constructor for the EnPassantPawnAttackMove class.
		 * 
		 * @param board
		 * @param movedPiece
		 * @param destinationCoordinate
		 * @param opposingPiece
		 */
		public EnPassantPawnAttackMove(Board board, Piece movedPiece, int destinationCoordinate, Piece opposingPiece) {
			super(board, movedPiece, destinationCoordinate, opposingPiece);
		}
		
		@Override
		public boolean equals(final Object other) {
			return this == other || other instanceof EnPassantPawnAttackMove && super.equals(other);
		}
		
		@Override
		public Board execute() {
			//System.out.println("Inside enpassant");
			final Builder builder = new Builder();
			final Builder tbuilder = new Builder();
			this.movedPiece.empass = false;
			for (final Piece piece : this.board.currentPlayer().getActivePieces()) {
				if (!this.movedPiece.equals(piece)) {
					piece.empass = false;
					builder.setPiece(piece);
					tbuilder.setPiece(piece);
				}
			}
			//System.out.println("Flag 1");
			int toLocation = this.movedPiece.movePiece(this).getPiecePosition();
			for (final Piece piece : this.board.currentPlayer().getOpponent().getActivePieces()) {
				piece.empass = false;
				boolean white = piece.getPieceAlliance().isWhite();
				System.out.println(toLocation);
				if (white && piece.getPiecePosition() + 8 == toLocation) {
					continue;
				} else if (!white && piece.getPiecePosition() - 8 == toLocation) {
					continue;
				}
				builder.setPiece(piece);
				tbuilder.setPiece(piece);
			}
			Piece moved = this.movedPiece.movePiece(this);
			Piece tpiece;
			Alliance side = this.board.currentPlayer().getAlliance();
			builder.setPiece(moved);
			
			if ((side == Alliance.BLACK && !board.blackCheck) || (side == Alliance.WHITE && !board.whiteCheck)) {
				tbuilder.setPiece(moved);
				tbuilder.setNextMove(this.board.currentPlayer().getOpponent().getAlliance());
				Boolean badmove = false;
				try {
					Board tempBoard = tbuilder.build();
					Collection<Move> moveList = tempBoard.getAllLegalMoves(tempBoard.currentPlayer().getAlliance().isBlack());
					for (Move mv : moveList) {
						if (tempBoard.getSquare(mv.getDestinationCoordinate()).isSquareOccupied()) {
							tpiece = tempBoard.getSquare(mv.getDestinationCoordinate()).getPiece();
							if (tpiece.getPieceType().equals(PieceType.KING) && tpiece.getPieceAlliance() == side) {
								badmove = true;
								throw new java.lang.RuntimeException();
							}
						}
					}
				} catch (java.lang.RuntimeException e) {
					if (badmove) {
						throw new java.lang.RuntimeException();
					} else {
						throw new java.lang.UnsupportedOperationException();
					}
				}
			
			}
			builder.setNextMove(this.board.currentPlayer().getOpponent().getAlliance());
			try {
				return builder.build();
			} catch (java.lang.RuntimeException e) {
				throw new java.lang.UnsupportedOperationException();
			}
		}
	}

	/**
	 * This subclass extends Move and represents the PawnJumpMove which identifies
	 * the pawn jumping two spaces.
	 */

	public static final class PawnJumpMove extends Move {
		/**
		 * Here is the constructor for the PawnJumpMove class.
		 * 
		 * @param board
		 * @param movedPiece
		 * @param destinationCoordinate
		 */
		public PawnJumpMove(final Board board, final Piece movedPiece, final int destinationCoordinate) {
			super(board, movedPiece, destinationCoordinate);
		}

		@Override
		public Board execute() {
			//System.out.println("Inside pawn jump");
			final Builder builder = new Builder();
			final Builder tbuilder = new Builder();
			for (final Piece piece : this.board.currentPlayer().getActivePieces()) {
				if (!this.movedPiece.equals(piece)) {
					piece.empass = false;
					builder.setPiece(piece);
					tbuilder.setPiece(piece);
				}
			}
			for (final Piece piece : this.board.currentPlayer().getOpponent().getActivePieces()) {
				piece.empass = false;
				builder.setPiece(piece);
				tbuilder.setPiece(piece);
			}
			//System.out.println("Half way..");
			final Pawn movedPawn = (Pawn) this.movedPiece.movePiece(this);
			Piece tpiece;
			Alliance side = this.board.currentPlayer().getAlliance();
			movedPawn.empass = true;
			builder.setPiece(movedPawn);
			
			if ((side == Alliance.BLACK && !board.blackCheck) || (side == Alliance.WHITE && !board.whiteCheck)) {
				tbuilder.setPiece(movedPawn);
				tbuilder.setNextMove(this.board.currentPlayer().getOpponent().getAlliance());
				Boolean badmove = false;
				try {
					Board tempBoard = tbuilder.build();
					Collection<Move> moveList = tempBoard.getAllLegalMoves(tempBoard.currentPlayer().getAlliance().isBlack());
					for (Move mv : moveList) {
						if (tempBoard.getSquare(mv.getDestinationCoordinate()).isSquareOccupied()) {
							tpiece = tempBoard.getSquare(mv.getDestinationCoordinate()).getPiece();
							if (tpiece.getPieceType().equals(PieceType.KING) && tpiece.getPieceAlliance() == side) {
								badmove = true;
								throw new java.lang.RuntimeException();
							}
						}
					}
				} catch (java.lang.RuntimeException e) {
					if (badmove) {
						throw new java.lang.RuntimeException();
					} else {
						throw new java.lang.UnsupportedOperationException();
					}
				}
		
			}
			//System.out.println("Made it here");
			builder.setNextMove(this.board.currentPlayer().getOpponent().getAlliance());
			builder.setEnPassantPawn(movedPawn);
			try {
				return builder.build();
			} catch (java.lang.RuntimeException e) {
				throw new java.lang.UnsupportedOperationException();
			}
		}
	}
	
	public static class PawnPromotion extends Move {
		
		final Move promotionMove; 
		final Pawn promotedPawn;
		
		public PawnPromotion(final Move promotionMove) {
			super(promotionMove.getBoard(), promotionMove.getMovedPiece(), promotionMove.getDestinationCoordinate());
			this.promotionMove = promotionMove;
			this.promotedPawn = (Pawn) promotionMove.getMovedPiece();
		}
		
		@Override
		public int hashCode() {
			return promotionMove.hashCode() + (31 + promotedPawn.hashCode());
		}
		
		@Override
		public boolean equals(final Object other) {
			return this == other || other instanceof PawnPromotion && super.equals(other);
		}
		
		@Override
		public Board execute() {
			System.out.println("Inside pawn promotion");
			//final Board pawnPromotionBoard = this.promotionMove.execute();
			final Board.Builder builder = new Builder();
			System.out.println(this.promotedPawn);
			System.out.println(this.promotedPawn.getPiecePosition());
			Pawn notforlong = (Pawn) this.getMovedPiece();
			Piece promotedPiece = Pawn.getPromotionPiece(this.getDestinationCoordinate(), this.getMovedPiece().getPieceAlliance(), notforlong.promoteTo);
			//final Builder tbuilder = new Builder();
			for (final Piece piece : board.currentPlayer().getActivePieces()) {
				if (!this.promotedPawn.equals(piece)) {
					piece.empass = false;
					builder.setPiece(piece);
				//	tbuilder.setPiece(piece);
				}
			}
			for (final Piece piece : this.board.currentPlayer().getOpponent().getActivePieces()) {
				piece.empass = false;
				builder.setPiece(piece);
				//tbuilder.setPiece(piece);
			}
			//builder.setPiece(this.promotedPawn.getPromotionPiece().movePiece(this));
			builder.setPiece(promotedPiece);
			builder.setNextMove(board.currentPlayer().getOpponent().getAlliance());
			return builder.build();
		}
		
		@Override 
		public boolean isAttack() {
			return this.promotionMove.isAttack();		
		}
		
		@Override 
		public Piece getAttackedPiece() {
			return this.promotionMove.getAttackedPiece();
		}
		
		@Override
		public String toString() {
			return "";
		}
	}

	/**
	 * This abstract class will identify the two possible castling moves.
	 */
	static abstract class CastleMove extends Move {

		/**
		 * These fields will be used to determine where to move the rook during
		 * castling.
		 */

		protected final Rook castleRook;
		protected final int castleRookPosition;
		protected final int castleRookDestination;

		/**
		 * Here is the constructor for the CastleMove class.
		 * 
		 * @param board
		 * @param movedPiece
		 * @param destinationCoordinate
		 * @param castleRook
		 * @param castleRookPosition
		 * @param castleRookDestination
		 */
		public CastleMove(final Board board, final Piece movedPiece, final int destinationCoordinate,
				final Rook castleRook, final int castleRookPosition, final int castleRookDestination) {
			super(board, movedPiece, destinationCoordinate);
			this.castleRook = castleRook;
			this.castleRookPosition = castleRookPosition;
			this.castleRookDestination = castleRookDestination;
		}

		/**
		 * This method returns the castled rook.
		 * 
		 * @return Rook
		 */
		public Rook getCastleRook() {
			return this.castleRook;
		}

		@Override
		public boolean isCastlingMove() {
			return true;
		}

		@Override
		public Board execute() {
			final Builder builder = new Builder();
			//final Builder tbuilder = new Builder();
			for (final Piece piece : this.board.currentPlayer().getActivePieces()) {
				if (!this.movedPiece.equals(piece) && !this.castleRook.equals(piece)) {
					piece.empass = false;
					builder.setPiece(piece);
				//	tbuilder.setPiece(piece);
				}
			}
			for (final Piece piece : this.board.currentPlayer().getOpponent().getActivePieces()) {
				piece.empass = false;
				builder.setPiece(piece);
				//tbuilder.setPiece(piece);
			}
//			Piece tpiece;
//			Alliance side = this.board.currentPlayer().getAlliance();
			Rook MovedRook = new Rook(this.castleRookDestination, this.getCastleRook().getPieceAlliance());
			King MovedKing = new King(this.destinationCoordinate, this.getCastleRook().getPieceAlliance(), false, false);
			builder.setPiece(MovedKing);
			builder.setPiece(MovedRook);
			//builder.setPiece(new Rook(this.castleRookDestination, this.castleRook.getPieceAlliance(), false)); // TODO
			
			builder.setNextMove(this.board.currentPlayer().getOpponent().getAlliance());
			return builder.build();

		}
		
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = super.hashCode();
			result = prime * result + this.castleRook.hashCode();
			result = prime * result + this.castleRookDestination;
			return result;
		}
		
		@Override
		public boolean equals (final Object other) {
			if (this == other) {
				return true;
			}
			
			if (!(other instanceof CastleMove)) {
				return false;
			}
			
			final CastleMove otherCastleMove = (CastleMove) other;
			return super.equals(otherCastleMove) && this.castleRook.equals(otherCastleMove.getCastleRook());
		}
	}

	/**
	 * This subclass extends CastleMove and will identify king side castle moves.
	 */

	public static final class KingSideCastleMove extends CastleMove {

		/**
		 * Here is the constructor for KingSideCastleMove class.
		 * 
		 * @param board
		 * @param movedPiece
		 * @param destinationCoordinate
		 */
		public KingSideCastleMove(Board board, Piece movedPiece, int destinationCoordinate, final Rook castleRook,
				final int castleRookPosition, final int castleRookDestination) {
			super(board, movedPiece, destinationCoordinate, castleRook, castleRookPosition, castleRookDestination);
		}
		
		@Override
        public boolean equals(final Object other) {
            if (this == other) {
                return true;
            }
            if (!(other instanceof KingSideCastleMove)) {
                return false;
            }
            final KingSideCastleMove otherKingSideCastleMove = (KingSideCastleMove) other;
            return super.equals(otherKingSideCastleMove) && this.castleRook.equals(otherKingSideCastleMove.getCastleRook());
        }
	}

	/**
	 * This subclass extends CastleMove and will identify queen side castle moves.
	 */

	public static final class QueenSideCastleMove extends CastleMove {

		/**
		 * Here is the constructor for the QueenSideCastleMove class.
		 * 
		 * @param board
		 * @param movedPiece
		 * @param destinationCoordinate
		 */
		public QueenSideCastleMove(Board board, Piece movedPiece, int destinationCoordinate, final Rook castleRook,
				final int castleRookPosition, final int castleRookDestination) {
			super(board, movedPiece, destinationCoordinate, castleRook, destinationCoordinate, destinationCoordinate);
		}
		
		@Override
        public boolean equals(final Object other) {
            if (this == other) {
                return true;
            }
            if (!(other instanceof QueenSideCastleMove)) {
                return false;
            }
            final QueenSideCastleMove otherQueenSideCastleMove = (QueenSideCastleMove) other;
            return super.equals(otherQueenSideCastleMove) && this.castleRook.equals(otherQueenSideCastleMove.getCastleRook());
        }
	}

	/**
	 * This subclass extends Move and will be used to identify invalid moves.
	 */
	public static final class NullMove extends Move {

		/**
		 * Here is the constructor for NullMove.
		 */
		public NullMove() {
			super(null, -1);
		}

		/**
		 * The method should not execute an invalid move.
		 */
		@Override
		public Board execute() {
			throw new RuntimeException("Cannot make this move");
		}

		@Override
		public int getCurrentCoordinate() {
			return -1;
		}

		@Override
		public int getDestinationCoordinate() {
			return -1;
		}
	}

	/**
	 * This inner class will create hold a createMove method to check for a null move.
	 */
	public static class MoveCreation {
		private MoveCreation() {
			throw new RuntimeException("Cannot be instantiated!");
		}

		private static final Move NULL_MOVE = new NullMove();

		public static Move getNullMove() {
			return NULL_MOVE;
		}

		/**
		 * This method will create the move that the player makes. If the move is not
		 * valid, then a null move will be returned.
		 * 
		 * @param board
		 * @param currentCoordinate
		 * @param desintationCoordinate
		 * @return move
		 */
		public static Move createMove(final Board board, final int currentCoordinate, final int desintationCoordinate) {
			boolean black;
			if (board.currentPlayer() == null) {
				black = false;
			} else {
				black = board.currentPlayer().getAlliance().isBlack();
			}
			for (final Move move : board.getAllLegalMoves(black)) {
				//System.out.println(move.getCurrentCoordinate() + " to " + move.getDestinationCoordinate());
				if (move.getCurrentCoordinate() == currentCoordinate
						&& move.getDestinationCoordinate() == desintationCoordinate) {
					return move;
				}
			}
			return NULL_MOVE;
		}
	}
}