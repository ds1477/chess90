package board;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import engine.Player;
import engine.PlayerBlack;
import engine.PlayerWhite;
import pieces.Piece;
import pieces.Bishop;
import pieces.King;
import pieces.Knight;
import pieces.Pawn;
import pieces.Queen;
import pieces.Rook;

/**
 * @author Daniel Samojlik and Elaine Yang
 *         <p>
 *         This Board class will be used to create the board with pieces via the
 *         builder pattern.
 */
public class Board {

	/**
	 * This variable will be the game board. A list is used to make the board
	 * immutable.
	 */
	private final List<Square> chessBoard;

	/**
	 * These variables will keep track of which pieces are in play on the board.
	 */
	private final Collection<Piece> whitePieces;
	private final Collection<Piece> blackPieces;

	/**
	 * These instances will help with making moves.
	 */

	private final PlayerWhite playerWhite;
	private final PlayerBlack playerBlack;
	private final Player currentPlayer; 
	
	/**
	 * This pawn will be used to identify an en passant pawn. 
	 */
	private final Pawn enPassantPawn;

	/**
	 * These variables will keep track of if any side is in check.
	 */
	public boolean whiteCheck = false;
	public boolean blackCheck = false;
	
	/**
	 * This constructor initializes the chess board from the Builder.
	 * 
	 * @param builder
	 */
	private Board(final Builder builder) {
		//System.out.println("Entered Board builder thing");
		this.chessBoard = createChessBoard(builder);
		this.whitePieces = piecesInPlay(this.chessBoard, Alliance.WHITE);
		this.blackPieces = piecesInPlay(this.chessBoard, Alliance.BLACK);

		/**
		 * These constructors call on the method to create a lists of legal moves for
		 * each side.
		 */
		final Collection<Move> whiteLegalMoves = calculateLegalMoves(this.whitePieces);
		final Collection<Move> blackLegalMoves = calculateLegalMoves(this.blackPieces);

		/**
		 * These constructors initialize which color each player plays as.
		 */

		this.playerWhite = new PlayerWhite(this, whiteLegalMoves, blackLegalMoves);
		this.playerBlack = new PlayerBlack(this, whiteLegalMoves, blackLegalMoves); 
		this.currentPlayer = builder.nextMove.choosePlayer(this.playerWhite, this.playerBlack);
		
		this.enPassantPawn = builder.enPassantPawn;
	}

	/**
	 * This toString method will print out the chess board.
	 * 
	 * @return String
	 */
	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		int row = 8;
		for (int i = 0; i < BoardMethods.Total_Squares; i++) {
			final String squareText = this.chessBoard.get(i).toString();
			builder.append(String.format("%3s", squareText));
			if ((i + 1) % 8 == 0) {
				builder.append(String.format("%3s", row));
				row--;
			}
			if ((i + 1) % BoardMethods.Squares_Per_Row == 0) {
				builder.append("\n");
			}
		}
		builder.append(String.format("%3s", "a"));
		builder.append(String.format("%3s", "b"));
		builder.append(String.format("%3s", "c"));
		builder.append(String.format("%3s", "d"));
		builder.append(String.format("%3s", "e"));
		builder.append(String.format("%3s", "f"));
		builder.append(String.format("%3s", "g"));
		builder.append(String.format("%3s", "h"));
		builder.append(String.format("%3s", ""));
		builder.append("\n");
		
		return builder.toString();
	}

	/**
	 * This method identifies the white player.
	 * 
	 * @return Player
	 */
	public Player playerWhite() {
		return this.playerWhite;
	}

	/**
	 * This method identifies the black player.
	 * 
	 * @return Player
	 */
	public Player playerBlack() {
		return this.playerBlack;
	}

	/**
	 * This method identifies the current player.
	 * 
	 * @return Player
	 */
	public Player currentPlayer() {
		return this.currentPlayer;
	}

	/**
	 * This method returns the collection of white pieces on a board.
	 * 
	 * @return whitePieces
	 */

	public Collection<Piece> getWhitePieces() {
		return this.whitePieces;
	}

	/**
	 * This method returns the collection of black pieces on a board.
	 * 
	 * @return blackPieces
	 */

	public Collection<Piece> getBlackPieces() {
		return this.blackPieces;
	}
	
	/**
	 * This method gets the en passant pawn. 
	 * @return
	 */
	public Pawn getEnPassantPawn() {
		return this.enPassantPawn;
	}

	/**
	 * This method iterates through each piece and adds the legal moves applicable
	 * to each piece.
	 * 
	 * @param pieces
	 * @return legalMoves legal moves for a given side
	 */
	private Collection<Move> calculateLegalMoves(final Collection<Piece> pieces) {
		final List<Move> legalMoves = new ArrayList<>();

		for (final Piece piece : pieces) {
			//System.out.println(piece.getPiecePosition());
			legalMoves.addAll(piece.calculateLegalMoves(this));
			//System.out.println("Considered moves of " + piece);
		}

		return Collections.unmodifiableList(legalMoves);
	}

	/**
	 * This method will calculate the board's active white and black pieces by
	 * checking the occupied squares and their residing pieces.
	 *
	 * @param board chess board
	 * @param color color of pieces
	 * @return activePieces list of pieces in play
	 */

	private Collection<Piece> piecesInPlay(final List<Square> board, final Alliance color) {
		final List<Piece> activePieces = new ArrayList<>();
		for (final Square square : board) {
			if (square.isSquareOccupied()) {
				final Piece piece = square.getPiece();
				if (piece.getPieceAlliance() == color) {
					activePieces.add(piece);
				}
			}
		}
		return Collections.unmodifiableList(activePieces);
	}

	/**
	 * This method populates a list of squares from 0 to 63 and associate a piece
	 * with its starting square.
	 * 
	 * @param builder
	 * @return squares list of squares, i.e. chess board
	 */
	private List<Square> createChessBoard(final Builder builder) {
		final Square[] squares = new Square[64];

		for (int i = 0; i < BoardMethods.Total_Squares; i++) {
			squares[i] = Square.createSquare(i, builder.boardSetup.get(i));
		}
		return Collections.unmodifiableList(Arrays.asList(squares));
	}

	/**
	 * This method will initialize all the board pieces at their starting positions
	 * and set white to move.
	 * 
	 * @return Board build method returns the newly initialized board
	 */
	public static Board initializeBoardPieces() {

		/**
		 * This instance of builder will call the methods in the Builder class to help
		 * create the board.
		 */
		final Builder builder = new Builder();

		builder.setPiece(new Rook(0, Alliance.BLACK));
		builder.setPiece(new Knight(1, Alliance.BLACK));
		builder.setPiece(new Bishop(2, Alliance.BLACK));
		builder.setPiece(new Queen(3, Alliance.BLACK));
		builder.setPiece(new King(4, Alliance.BLACK, true, true));
		builder.setPiece(new Bishop(5, Alliance.BLACK));
		builder.setPiece(new Knight(6, Alliance.BLACK));
		builder.setPiece(new Rook(7, Alliance.BLACK));
		for(int i = 8; i < 16; i ++) {
			builder.setPiece(new Pawn(i, Alliance.BLACK));
		}

		for(int i = 48; i < 56; i ++) {
			builder.setPiece(new Pawn(i, Alliance.WHITE));
		}
	
		builder.setPiece(new Rook(56, Alliance.WHITE));
		builder.setPiece(new Knight(57, Alliance.WHITE));
		builder.setPiece(new Bishop(58, Alliance.WHITE));
		builder.setPiece(new Queen(59, Alliance.WHITE));
		builder.setPiece(new King(60, Alliance.WHITE, true, true));
		builder.setPiece(new Bishop(61, Alliance.WHITE));
		builder.setPiece(new Knight(62, Alliance.WHITE));
		builder.setPiece(new Rook(63, Alliance.WHITE));

		builder.setNextMove(Alliance.WHITE);
		return builder.build();
	}

	/**
	 * This method will be used to obtain the square of a coordinate.
	 * 
	 * @param coordinate
	 * @return square
	 */
	public Square getSquare(final int coordinate) {
		return chessBoard.get(coordinate);
	}

	/**
	 * This inner class will be used to create an instance of the board. This class
	 * will use mutable methods to create the board.
	 */
	public static class Builder {

		/**
		 * This mapping is used to map coordinates on the board, from 0 through 63, to
		 * the respective pieces.
		 */
		Map<Integer, Piece> boardSetup;
		

		/**
		 * This variable will be used to keep track of the player's turn to move.
		 */
		Alliance nextMove;
		
		/**
		 * This pawn will be used to identify an en passant pawn. 
		 */
		Pawn enPassantPawn;
		
		/**
		 * This no-arg Builder constructor assigns a hash map to boardSetup.
		 */

		public Builder() {
			this.boardSetup = new HashMap<>();
		}

		/**
		 * This method will set the piece at its respective starting position.
		 * 
		 * @param piece
		 * @return Builder returns Builder to where it was called from
		 */
		public Builder setPiece(final Piece piece) {
			this.boardSetup.put(piece.getPiecePosition(), piece);
			return this;
		}

		/**
		 * This method will set whose move it is.
		 * 
		 * @param nextMove
		 * @return Builder returns Builder to where it was called from
		 */
		public Builder setNextMove(final Alliance nextMove) {
			this.nextMove = nextMove;
			return this;
		}

		/**
		 * This method will build and return the complete chess board.
		 * 
		 * @return Board creates an immutable board
		 */
		public Board build() {
			return new Board(this);
		}

		/**
		 * This method sets the en passant pawn when an en passant occurs.
		 * @param enPassantPawn
		 */
		public Builder setEnPassantPawn(final Pawn enPassantPawn) {
			this.enPassantPawn = enPassantPawn;
			return this;
			
		}
	}
	/**
	 * This method concatenates all the legal moves of both players.
	 * @return Collection<Move>
	 */
	public Collection<Move> getAllLegalMoves(boolean isBlack) {
		if (isBlack) {
			return Collections.unmodifiableList(this.playerBlack.getLegalMoves().stream().collect(Collectors.toList()));
		} else {
			return Collections.unmodifiableList(this.playerWhite.getLegalMoves().stream().collect(Collectors.toList()));
		}
		
	}

}