package board;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import engine.MoveTransition;

/**
 * @author Elaine Yang & Daniel Samojlik This class contains static methods that
 *         different pieces can use to check for legal moves.
 *
 */
public class BoardMethods {

	/**
	 * This class cannot be instantiated.
	 */
	private BoardMethods() {
		throw new RuntimeException("You cannot instantiate this class");
	}

	/**
	 * These variables are used to initialize the squares in a board and in a row.
	 */
	public static final int Total_Squares = 64;
	public static final int Squares_Per_Row = 8;

	/**
	 * This method checks if the input coordinate exists on the board.
	 * 
	 * @param possibleCoordinate
	 * @return boolean
	 */
	public static boolean doesCoordinateExist(final int possibleCoordinate) {
		return (possibleCoordinate >= 0 && possibleCoordinate < Total_Squares);
	}

	/**
	 * These boolean arrays are used to initialize the columns to true. These are
	 * used for the edge cases for the knight, bishop, rook, and queen pieces.
	 */
	public static final boolean[] First_Column = initColumn(0);
	public static final boolean[] Second_Column = initColumn(1);
	public static final boolean[] Seventh_Column = initColumn(6);
	public static final boolean[] Eighth_Column = initColumn(7);

	/**
	 * These boolean arrays are used to initialize the ranks to true. These are
	 * used for the edge cases for the pawn pieces and pawn promotion.
	 */
	public static final boolean[] First_Row = initRank(0);
	public static final boolean[] Second_Row = initRank(8);
	public static final boolean[] Third_Row = initRank(16);
	public static final boolean[] Fourth_Row = initRank(24);
	public static final boolean[] Fifth_Row = initRank(32);
	public static final boolean[] Sixth_Row = initRank(40);
	public static final boolean[] Seventh_Row = initRank(48);
	public static final boolean[] Eighth_Row = initRank(56);

	/**
	 * This method is used initialize all the the squares in a column to true via a
	 * do while loop.
	 * 
	 * @param columnNumber
	 * @return boolean array
	 */
	private static boolean[] initColumn(int columnNumber) {
		final boolean[] column = new boolean[Total_Squares];
		do {
			column[columnNumber] = true;
			columnNumber += Squares_Per_Row;
		} while (columnNumber < Total_Squares);

		return column;
	}

	/**
	 * This method is used initialize all the the squares in a row to true via a do
	 * while loop.
	 * 
	 * @param columnNumber
	 * @return boolean array
	 */
	private static boolean[] initRank(int rowNumber) {
		final boolean[] row = new boolean[Total_Squares];
		do {
			row[rowNumber] = true;
			rowNumber++;
		} while (rowNumber % Squares_Per_Row != 0);

		return row;
	}
	
	/**
	 * These fields will allow for the mapping between coordinates and the chess notation of the board.
	 */
	public static final List<String> CHESS_NOTATION = initializeRankColumnNotation();
    public static final Map<String, Integer> POSITION_TO_COORDINATE = initializePositionToCoordinateMap();

    /**
     * This will return the initialization of a chess board in a list with ranks and columns.
     * @return List<String>
     */
	private static List<String> initializeRankColumnNotation() {
		 return Collections.unmodifiableList(Arrays.asList(
	                "a8", "b8", "c8", "d8", "e8", "f8", "g8", "h8",
	                "a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7",
	                "a6", "b6", "c6", "d6", "e6", "f6", "g6", "h6",
	                "a5", "b5", "c5", "d5", "e5", "f5", "g5", "h5",
	                "a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4",
	                "a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3",
	                "a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2",
	                "a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1"));
	}

	/**
	 * This method maps the coordinate from 0 to 63 with the respective rank and column position.
	 * @return Map<String, Integer>
	 */
	private static Map<String, Integer> initializePositionToCoordinateMap() {
		final Map<String, Integer> positionToCoordinate = new HashMap<>();
        for (int i = 0; i < Total_Squares; i++) {
            positionToCoordinate.put(CHESS_NOTATION.get(i), i);
        }
        return Collections.unmodifiableMap(positionToCoordinate);
	}

	/**
	 * This method will return a coordinate from 0 to 63 that maps the the input chess notation.
	 * @param position
	 * @return coordinate 
	 */
	public static int getCoordinateAtPosition(final String position) {
        return POSITION_TO_COORDINATE.get(position);
    }

	/**
	 * This method will return the chess notation of the respective coordinate input
	 * @param coordinate
	 * @return position string of the rank and column
	 */
    public static String getPositionAtCoordinate(final int coordinate) {
        return CHESS_NOTATION.get(coordinate);
    }
    
    /**
     * This method checks if a move will leave the current player in check,
     * @param move
     * @return boolean
     */
    
    public static boolean kingThreat(final Move move) {
        final Board board = move.getBoard();
        final MoveTransition transition = board.currentPlayer().makeMove(move);
        return transition.getTransitionBoard().currentPlayer().isInCheck();
    }
}
