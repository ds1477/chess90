package engine;

/**
 * @author Daniel Samojlik and Elaine Yang 
 * <p>
 * This MoveStatus enum will give the
 * status of a move.
 */

public enum MoveStatus {
	DONE {
		@Override
		boolean isDone() {
			return true;
		}
	},
	ILLEGAL_MOVE {
		@Override
		boolean isDone() {
			return false;
		}
	},
	LEAVES_PLAYER_IN_CHECK {
		@Override
		boolean isDone() {
			return false;
		}
	};

	/**
	 * This abstract method checks if the method is done.
	 * 
	 * @return boolean
	 */
	abstract boolean isDone();
}
