package engine;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import board.Alliance;
import board.Board;
import board.Move;
import board.Square;
import pieces.Piece;
import pieces.Rook;

/**
 * @author Daniel Samojlik & Elaine Yang 
 * <p>
 * This class extends the Player class and
 * is used for the white pieces.
 */

public class PlayerWhite extends Player {

	/**
	 * Here is the constructor for the class. Notice that the blackLegalMoves are
	 * the legalMoves for the black player.
	 * 
	 * @param board
	 * @param whiteLegalMoves
	 * @param blackLegalMoves
	 */
	public PlayerWhite(final Board board, final Collection<Move> whiteLegalMoves,
			final Collection<Move> blackLegalMoves) {
		super(board, whiteLegalMoves, blackLegalMoves);
	}

	@Override
	public Collection<Piece> getActivePieces() {
		return this.board.getWhitePieces();
	}

	@Override
	public Alliance getAlliance() {
		return Alliance.WHITE;
	}

	@Override
	public Player getOpponent() {
		return this.board.playerBlack();
	}

	@Override
	protected Collection<Move> calculateKingCastleMoves(final Collection<Move> playerLegalMoves,
			final Collection<Move> opponentLegalMoves) {
		
		 if(!canCastle()) {
	            return Collections.emptyList();
	     }

		final List<Move> kingCastles = new ArrayList<>();

		if (this.playerKing.isFirstMove() && !this.isInCheck() && this.playerKing.getPiecePosition() == 60) {
			if (!this.board.getSquare(61).isSquareOccupied() && !this.board.getSquare(62).isSquareOccupied()) {
				final Square rookSquare = this.board.getSquare(63);
				if (rookSquare.isSquareOccupied() && rookSquare.getPiece().isFirstMove()
						&& Player.calculateAttacksOnTile(61, opponentLegalMoves).isEmpty()
						&& Player.calculateAttacksOnTile(62, opponentLegalMoves).isEmpty()
						&& rookSquare.getPiece().getPieceType().isRook()) {
					kingCastles.add(new Move.KingSideCastleMove(this.board, this.playerKing, 62, (Rook) rookSquare.getPiece(),
							rookSquare.getSquareCoordinate(), 61));
				}
			}
			
			if (!this.board.getSquare(57).isSquareOccupied() && !this.board.getSquare(58).isSquareOccupied()
					&& !this.board.getSquare(59).isSquareOccupied()) {
				final Square rookSquare = this.board.getSquare(56);
				if (rookSquare.isSquareOccupied() && rookSquare.getPiece().isFirstMove()
						&& Player.calculateAttacksOnTile(57, opponentLegalMoves).isEmpty()
						&& Player.calculateAttacksOnTile(58, opponentLegalMoves).isEmpty()
						&& Player.calculateAttacksOnTile(59, opponentLegalMoves).isEmpty()
						&& rookSquare.getPiece().getPieceType().isRook()) {
					kingCastles.add(new Move.QueenSideCastleMove(this.board, this.playerKing, 58, (Rook) rookSquare.getPiece(),
							rookSquare.getSquareCoordinate(), 59));
				}
			}
		}
		return Collections.unmodifiableList(kingCastles);
	}
}
