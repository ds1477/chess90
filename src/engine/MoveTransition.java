package engine;

import board.Board;
import board.Move;

/**
 * @author Daniel Samojlik & Elaine Yang 
 * <p>
 * This class will be used to represent
 * the transition of one board to another when a move is made, so that
 * all the information of the board is captured.
 */
public class MoveTransition {

	/**
	 * This Board instance the current board.
	 */

	private final Board currentBoard;

	/**
	 * This Board instance will represent a hypothetical board after the move is
	 * made.
	 */

	private final Board transitionBoard;

	/**
	 * This variable is the move made.
	 */

	private final Move move;

	/**
	 * This enum tells status of the move.
	 */

	private final MoveStatus moveStatus;

	/**
	 * Here is the constructor for the MoveTransition class.
	 * 
	 * @param currentBoard
	 * @param transitionBoard
	 * @param move
	 * @param moveStatus
	 */
	public MoveTransition(final Board currentBoard, final Board transitionBoard, final Move move,
			final MoveStatus moveStatus) {
		this.currentBoard = currentBoard;
		this.transitionBoard = transitionBoard;
		this.move = move;
		this.moveStatus = moveStatus;
	}
	
	/**
	 * This method gets the current board.
	 * @return Board
	 */

	public Board getCurrentBoard() {
		return this.currentBoard;
	}
	
	/**
	 * This method gets the transition board.
	 * @return Board
	 */

	public Board getTransitionBoard() {
		return this.transitionBoard;
	}

	/**
	 * This method gets the move being made.
	 * @return move
	 */
	public Move getMove() {
		return this.move;
	}

	/**
	 * This method retrieves the move status from the MoveStatus enum.
	 * 
	 * @return MoveStatus
	 */
	public MoveStatus getMoveStatus() {
		return this.moveStatus;
	}
}
