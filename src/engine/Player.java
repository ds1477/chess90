package engine;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import board.Alliance;
import board.Board;
import board.BoardMethods;
import board.Move;
import pieces.King;
import pieces.Piece;

/**
 * @author Daniel Samojlik & Elaine Yang 
 * <p>
 * This Player class is an abstract class
 * and will be utilized to help determine castling moves for the
 * players.
 */

public abstract class Player {

	/**
	 * These fields establish the Player class.
	 */
	protected final Board board;
	protected final King playerKing;
	protected final Collection<Move> legalMoves;
	private final boolean isInCheck;

	/**
	 * Here is the constructor for the Player class.
	 * 
	 * @param board
	 * @param legalMoves
	 * @param opponentMoves
	 */

	Player(final Board board, final Collection<Move> legalMoves, final Collection<Move> opponentMoves) {
		this.board = board;
		this.playerKing = establishKing();
		this.legalMoves = Stream.concat(legalMoves.stream(), calculateKingCastleMoves(legalMoves, opponentMoves).stream())
                .collect(Collectors.toList());
		this.isInCheck = !Player.calculateAttacksOnTile(this.playerKing.getPiecePosition(), opponentMoves).isEmpty();
	}

	/**
	 * This method will be used to determine if the destination coordinate of the
	 * enemy move overlaps with the kings position.
	 * 
	 * @param piecePosition
	 * @param opponentMoves
	 * @return attackMoves if list is not empty, then king is in check
	 */
	protected static Collection<Move> calculateAttacksOnTile(int piecePosition, Collection<Move> opponentMoves) {
		final List<Move> attackMoves = new ArrayList<>();
		for (final Move move : opponentMoves) {
			if (piecePosition == move.getDestinationCoordinate()) {
				attackMoves.add(move);
			}
		}
		return Collections.unmodifiableList(attackMoves);
	}

	/**
	 * This method gets the current player's king.
	 * 
	 * @return King
	 */
	public King getPlayerKing() {
		return this.playerKing;
	}

	/**
	 * This method gets the list of legal moves.
	 * 
	 * @return Collection<Move>
	 */

	public Collection<Move> getLegalMoves() {
		return this.legalMoves;
	}

	/**
	 * This method finds the King of the player. If it is not found, then a runtime
	 * exception is thrown because if there is no king, then the game is over.
	 * 
	 * @return King
	 */
	private King establishKing() {
		for (final Piece piece : getActivePieces()) {
			if (piece.getPieceType().isKing()) {
				return (King) piece;
			}
		}
		throw new RuntimeException("This is not a valid board!");
	}

	/**
	 * This method checks if a move is legal by checking the legalMoves list.
	 * 
	 * @param move
	 * @return boolean
	 */

	public boolean isMoveLegal(final Move move) {
		return this.legalMoves.contains(move); // TODO later
	}

	/**
	 * This method checks if the the king is in check.
	 * 
	 * @return boolean
	 */

	public boolean isInCheck() {
		return this.isInCheck;
	}

	/**
	 * This method checks if the the king is in checkmate.
	 * 
	 * @return boolean
	 */

	public boolean isInCheckMate() {
		return this.isInCheck && !hasEscapeMoves();
	}
	
	/**
	 * This method checks if a king has castled.
	 * @return boolean
	 */

	public boolean isCastled() {
        return this.playerKing.isCastled();
    }
	
	/**
	 * This method checks if a king is able to king-side castle.
	 * @return boolean
	 */

    public boolean canKingSideCastle() {
    	return this.playerKing.canKingSideCastle();
    }

    /**
     * This method checks if a king is able to queen-side castle.
     * @return boolean
     */
    public boolean canQueenSideCastle() {
    	return this.playerKing.canQueenSideCastle();
    }    
    
	/**
	 * This method will check if the king can escape from check. This is done
	 * through taking the legal moves and doing them on another board to see if that
	 * move is legal.
	 * 
	 * @return boolean
	 */
	protected boolean hasEscapeMoves() {
		for (final Move move : this.legalMoves) {
			final MoveTransition transition = makeMove(move);
			if (transition.getMoveStatus().isDone()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * This method will check whether the move is legal. If not, it will not create
	 * a new transition board. If the move is legal (with no attacks on the King),
	 * then a new transition board is created.
	 * 
	 * @param move
	 * @return MoveTransition abstract board
	 */
	public MoveTransition makeMove(final Move move) {
		if (!this.legalMoves.contains(move) || BoardMethods.kingThreat(move)) {
			return new MoveTransition(this.board, this.board, move, MoveStatus.ILLEGAL_MOVE);
		}

		final Board transitionBoard = move.execute();
		
		final Collection<Move> kingAttacks = Player.calculateAttacksOnTile(
				board.currentPlayer().getOpponent().getPlayerKing().getPiecePosition(), 
				board.currentPlayer().getLegalMoves());
		
		if (!kingAttacks.isEmpty()) {
			return new MoveTransition(this.board, this.board, move, MoveStatus.LEAVES_PLAYER_IN_CHECK);
		} else {
			return new MoveTransition(this.board, transitionBoard, move, MoveStatus.DONE);
			
		}
	}
	
	/**
	 * This method checks is a castle is possible. 
	 * @return boolean
	 */
	
	protected boolean canCastle() {
        return !this.isInCheck && !this.playerKing.isCastled() &&
                (this.playerKing.canKingSideCastle() || this.playerKing.canQueenSideCastle());
    }
	
	/**
	 * This abstract method when called will retrieve the active pieces on the board
	 * of a given color.
	 * 
	 * @return Collection<Piece>
	 */
	public abstract Collection<Piece> getActivePieces();

	/**
	 * This abstract method when called will retrieve the player's alliance, i.e.
	 * piece color.
	 * 
	 * @return Alliance
	 */

	public abstract Alliance getAlliance();

	/**
	 * This abstract method when called will return the player's opponent.
	 * 
	 * @return Player
	 */
	public abstract Player getOpponent();
	
	/**
	 * This abstract method will find the valid castling moves for the king and queen sides.
	 * @param playerLegalMoves
	 * @param opponentLegalMoves
	 * @return Collection<Move>
	 */
	protected abstract Collection<Move> calculateKingCastleMoves(Collection<Move> playerLegalMoves, Collection<Move> opponentLegalMoves);
}
