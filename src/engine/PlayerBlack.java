package engine;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import board.Alliance;
import board.Board;
import board.Move;
import board.Square;
import pieces.Piece;
import pieces.Rook;

/**
 * @author Daniel Samojlik & Elaine Yang 
 * <p> 
 * This class extends the Player class and
 * is used for the black pieces.
 */

public class PlayerBlack extends Player {

	/**
	 * Here is the constructor for the class. Notice that the blackLegalMoves are
	 * the legalMoves for the black player.
	 * 
	 * @param board
	 * @param whiteLegalMoves
	 * @param blackLegalMoves
	 */
	public PlayerBlack(final Board board, final Collection<Move> whiteLegalMoves,
			final Collection<Move> blackLegalMoves) {
		super(board, blackLegalMoves, whiteLegalMoves);
	}

	@Override
	public Collection<Piece> getActivePieces() {
		return this.board.getBlackPieces();
	}

	@Override
	public Alliance getAlliance() {
		return Alliance.BLACK;
	}

	@Override
	public Player getOpponent() {
		return this.board.playerWhite();
	}

	@Override
	protected Collection<Move> calculateKingCastleMoves(final Collection<Move> playerLegalMoves,
			final Collection<Move> opponentLegalMoves) {
		
		if(!canCastle()) {
            return Collections.emptyList();
		}
		
		final List<Move> kingCastles = new ArrayList<>();

		if (this.playerKing.isFirstMove() && !this.isInCheck()) {
			if (!this.board.getSquare(5).isSquareOccupied() && !this.board.getSquare(6).isSquareOccupied()) {
				final Square rookSquare = this.board.getSquare(7);
				if (rookSquare.isSquareOccupied() && rookSquare.getPiece().isFirstMove()
						&& Player.calculateAttacksOnTile(5, opponentLegalMoves).isEmpty()
						&& Player.calculateAttacksOnTile(6, opponentLegalMoves).isEmpty()
						&& rookSquare.getPiece().getPieceType().isRook()) {
					kingCastles.add(new Move.KingSideCastleMove(this.board, this.playerKing, 6,
							(Rook) rookSquare.getPiece(), rookSquare.getSquareCoordinate(), 5));
				}
			}
			
			if (!this.board.getSquare(1).isSquareOccupied() && !this.board.getSquare(2).isSquareOccupied()
					&& !this.board.getSquare(3).isSquareOccupied()) {
				final Square rookSquare = this.board.getSquare(0);
				if (rookSquare.isSquareOccupied() && rookSquare.getPiece().isFirstMove()
						&& Player.calculateAttacksOnTile(1, opponentLegalMoves).isEmpty()
						&& Player.calculateAttacksOnTile(2, opponentLegalMoves).isEmpty()
						&& Player.calculateAttacksOnTile(3, opponentLegalMoves).isEmpty()
						&& rookSquare.getPiece().getPieceType().isRook()) {
			
					kingCastles.add(new Move.QueenSideCastleMove(this.board, this.playerKing, 2,
							(Rook) rookSquare.getPiece(), rookSquare.getSquareCoordinate(), 3));
				}
			}
		}
		return Collections.unmodifiableList(kingCastles);
	}
}
