package chess;

import java.util.Collection;
import java.util.Scanner;

import board.Board;
import board.BoardMethods;
import board.Move;
import board.Square;
import engine.MoveTransition;
import engine.Player;
import engine.PlayerBlack;
import pieces.Pawn;
import pieces.Piece;
import pieces.Piece.PieceType;


/**
 * @author Elaine Yang and Daniel Samojlik
 * This Chess class contains the main method and will run the chess game. 
 */

public class Chess {

	public static void main(String[] args) {

		Board board = Board.initializeBoardPieces();
		
		System.out.println(board);
		
		String turn = "White";
		Scanner input = new Scanner(System.in);
		String move;
		int from;
		int toCoord;
		Move moveTo;
		Square sqr;
		Boolean gameover = false;
		String thirdArg;
		Boolean drawState = false;
		Boolean illegal = false;
		Collection<Move> moveList;
		
		while(!gameover) { 
			if (!illegal) {
				moveList = board.getAllLegalMoves(turn.equals("White"));
				for (Move mv : moveList) {
					if (board.getSquare(mv.getDestinationCoordinate()).isSquareOccupied()) {
						if (board.getSquare(mv.getDestinationCoordinate()).getPiece().getPieceType().equals(PieceType.KING)) {
							System.out.println("Check");
							if (turn.equals("White")) {
								//System.out.println("White check is true");
								board.whiteCheck = true;
							} else {
								//System.out.println("Black check is true");
								board.blackCheck = true;
							}
							break;
						}
					}
				}
				moveList = board.getAllLegalMoves(turn.equals("Black"));
				for (Move mv : moveList) {
					if (board.getSquare(mv.getDestinationCoordinate()).isSquareOccupied()) {
						if (board.getSquare(mv.getDestinationCoordinate()).getPiece().getPieceType().equals(PieceType.KING)) {
							if (turn.equals("White")) {
								board.blackCheck = true;
							} else {
								board.whiteCheck = true;
							}
							break;
						}
					}
				}
			}
			System.out.println(turn + "'s Move:");
			move = input.nextLine();
			if (move.equals("resign")) {
				if (turn.equals("White")) {
					System.out.println("Black wins");
				} else {
					System.out.println("White wins");
				}
				break;
			} else if (move.equals("draw")) {
				if (drawState) {
					break;
				} else {
					System.out.println("Illegal move, try again");
					illegal = true;
					continue;
				}
			}
			from = BoardMethods.getCoordinateAtPosition(move.split(" ")[0]);
			sqr = board.getSquare(from);
			toCoord = BoardMethods.getCoordinateAtPosition(move.split(" ")[1]);
			
			if (sqr.isSquareOccupied()) {
				try {
					//System.out.println("White check is now " + board.whiteCheck);
					//System.out.println("Black check is now " + board.blackCheck);
					moveTo = Move.MoveCreation.createMove(board, from, toCoord);
					if (move.split(" ").length == 3) {
						thirdArg = move.split(" ")[2];
						if (!thirdArg.equals("draw?")) {
							Pawn movedPiece = (Pawn) board.getSquare(from).getPiece();
							movedPiece.promoteTo = thirdArg;
							board = moveTo.execute();
						} else {
							board = moveTo.execute();
							drawState = true;
						}
					} else {
						board = moveTo.execute();
					}
						
				} catch (java.lang.UnsupportedOperationException e) {
					System.out.println("Checkmate");
					if (turn.equals("White")) {
						System.out.println("White wins");
					} else {
						System.out.println("Black wins");
					}
					break;
				
			    } catch (java.lang.RuntimeException e) {
					Collection<Piece> otherPieces;
					if (turn.equals("White")) {
						otherPieces = board.getBlackPieces();
					} else {
						otherPieces = board.getWhitePieces();
					}
					boolean king = false;
					for (Piece p : otherPieces) {
						if (p.getPieceType().equals(PieceType.KING)) {
							king = true;
						}
					}
					if (!king) {
						System.out.println("Checkmate");
						if (turn.equals("White")) {
							System.out.println("White wins");
						} else {
							System.out.println("Black wins");
						}
						break;
					}
					illegal = true;
					System.out.println("Illegal move, try again");
					continue;
				}
			} else {
				illegal = true;
				System.out.println("Illegal move, try again");
				continue;
			}
			if (turn.equals("White")) {
				turn = "Black";
				board.whiteCheck = false;
			} else {
				turn = "White";
				board.blackCheck = false;
			}
			illegal = false;
			System.out.println(""); //blank line before next board
			System.out.println(board);
		}
		
		
		input.close();
		}
}