package pieces;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import board.Board;
import board.BoardMethods;
import board.Move;
import board.Move.EnPassantPawnAttackMove;
import board.Move.PawnAttackMove;
import board.Move.PawnJumpMove;
import board.Move.PawnMove;
import board.Move.PawnPromotion;
import engine.Player;
import board.Alliance;

/**
 * @author Daniel Samojlik & Elaine Yang
 *         <p>
 *         This Pawn class extends the Piece class and will be used to calculate
 *         the legal moves of the pawn piece.
 */
public class Pawn extends Piece {

	public String promoteTo = "Q";
	
	/**
	 * Here is the constructor for an initial pawn piece.
	 * 
	 * @param piecePosition
	 * @param pieceAlliance
	 */
	public Pawn(final int piecePosition, final Alliance pieceAlliance) {
		super(PieceType.PAWN, piecePosition, pieceAlliance, true);
	}
	
	/**
	 * Here is the constructor for subsequent pawn pieces.
	 * @param piecePosition
	 * @param pieceAlliance
	 * @param isFirstMove
	 */
	
	public Pawn (final int piecePosition, final Alliance pieceAlliance, final boolean isFirstMove) {
		super(PieceType.PAWN, piecePosition, pieceAlliance, isFirstMove);
	}


	/**
	 * Here is the abstract method implementation for legal moves of the pawn.
	 * 
	 * @param board
	 * @return ArrayList
	 */
	@Override
	public List<Move> calculateLegalMoves(final Board board) {

		/**
		 * An array list will be used to store all the legal moves that can be made.
		 */

		final List<Move> legalMoves = new ArrayList<>();

		/**
		 * This set of code determines the difference possible moves for the pawn and adds the valid moves respectively.
		 */

		int position = this.piecePosition;
		Piece piece = this;
		
		if (this.pieceAlliance.isWhite()) {
			
			if (position < 56 && position > 47) {
				legalMoves.add(new Move.PawnJumpMove(board, piece, position - 16));
			}
			
			if (position - 8 >= 0 && !board.getSquare(position - 8).isSquareOccupied()) {
				if (position - 8 < 8) {
					legalMoves.add(new Move.PawnPromotion(new Move.NonAttackingMove(board, piece, position - 8)));
				} else {
					legalMoves.add(new Move.NonAttackingMove(board, piece, position - 8));
				}
			}
			
			if (position - 7 >= 0) {
				if (board.getSquare(position-7).isSquareOccupied()) {
					if (board.getSquare(position-7).getPiece().getPieceAlliance().isBlack()) {
						if (position - 7 < 8) {
							legalMoves.add(new Move.PawnPromotion(new Move.PawnAttackMove(board, piece, position - 7, board.getSquare(position-7).getPiece())));
						}
						else {				
							legalMoves.add(new Move.PawnAttackMove(board, piece, position-7, board.getSquare(position-7).getPiece()));
						}
					}
				}
			}
			
			if (position - 9 >= 0) {
				if (board.getSquare(position-9).isSquareOccupied()) {
					if (board.getSquare(position-9).getPiece().getPieceAlliance().isBlack()) {
						if (position - 9 < 8) {
							legalMoves.add(new Move.PawnPromotion(new Move.PawnAttackMove(board, piece, position - 9, board.getSquare(position-9).getPiece())));
						}
						else {				
							legalMoves.add(new Move.PawnAttackMove(board, piece, position-9, board.getSquare(position-9).getPiece()));
						}
					}
				}
			}
			
			if (board.getSquare(position + 1).isSquareOccupied()) {
				final Piece pieceOnCoordinate = board.getSquare(position + 1).getPiece();
				if(this.pieceAlliance != pieceOnCoordinate.getPieceAlliance() && pieceOnCoordinate.empass) {
					legalMoves.add(new EnPassantPawnAttackMove(board, this, position - 7, pieceOnCoordinate));
				}
			}
			if (board.getSquare(position - 1).isSquareOccupied()) {
				final Piece pieceOnCoordinate = board.getSquare(position - 1).getPiece();
				if(this.pieceAlliance != pieceOnCoordinate.getPieceAlliance() && pieceOnCoordinate.empass) {
					legalMoves.add(new EnPassantPawnAttackMove(board, this, position - 9, pieceOnCoordinate));
				}
			}
		} else {
			if (position > 7 && position < 16 ) {
				legalMoves.add(new Move.PawnJumpMove(board, piece, position + 16));
			}
			
			if (position + 8 < 64 && !board.getSquare(position + 8).isSquareOccupied()) {
				if (position+8 > 55) {
					legalMoves.add(new Move.PawnPromotion(new Move.NonAttackingMove(board, piece, position + 8)));
				} else {
					legalMoves.add(new Move.NonAttackingMove(board, piece, position + 8));
				}
			}
			
			if (position + 7 < 64) {
				if (board.getSquare(position+7).isSquareOccupied()) {
					if (board.getSquare(position+7).getPiece().getPieceAlliance().isWhite()) {
						if (position + 7 > 55) {
							legalMoves.add(new Move.PawnPromotion(new Move.PawnAttackMove(board, piece, position + 7, board.getSquare(position+7).getPiece())));
						}
						else {
							legalMoves.add(new Move.PawnAttackMove(board, piece, position+7, board.getSquare(position+7).getPiece()));
						}
					}
				}
			}
			
			if (position + 9 < 64) {
				if (board.getSquare(position+9).isSquareOccupied()) {
					if (board.getSquare(position+9).getPiece().getPieceAlliance().isWhite()) {
						if (position + 9 > 55) {
							legalMoves.add(new Move.PawnPromotion(new Move.PawnAttackMove(board, piece, position + 9, board.getSquare(position+9).getPiece())));
						}
						else {
							legalMoves.add(new Move.PawnAttackMove(board, piece, position+9, board.getSquare(position+9).getPiece()));
						}
					}
				}
			}
			
			if (board.getSquare(position + 1).isSquareOccupied()) {
				final Piece pieceOnCoordinate = board.getSquare(position + 1).getPiece();
				if(this.pieceAlliance != pieceOnCoordinate.getPieceAlliance() && pieceOnCoordinate.empass) {
					legalMoves.add(new EnPassantPawnAttackMove(board, this, position + 9, pieceOnCoordinate));
				}
			}
			if (board.getSquare(position - 1).isSquareOccupied()) {
				final Piece pieceOnCoordinate = board.getSquare(position - 1).getPiece();
				if(this.pieceAlliance != pieceOnCoordinate.getPieceAlliance() && pieceOnCoordinate.empass) {
					legalMoves.add(new EnPassantPawnAttackMove(board, this, position + 7, pieceOnCoordinate));
				}
			}
		}
		
//		for (Move move : legalMoves) {
//			System.out.println(move.getCurrentCoordinate() + " to " + move.getDestinationCoordinate());
//		}
		
		return Collections.unmodifiableList(legalMoves);
	}
	/**
	 * This method prints the String representation of the piece onto the board. 
	 * @return String
	 */
	
	@Override
	public String toString() {
		if (this.pieceAlliance.isWhite()) {
			return "w" + PieceType.PAWN.toString();
		}
		else {
			return "b" + PieceType.PAWN.toString();
		}
	}
	
	@Override
	public Piece movePiece(Move move) {
		return new Pawn(move.getDestinationCoordinate(), move.getMovedPiece().getPieceAlliance(), false);
	}
	
	/**
	 * This method returns a new piece when a promotion takes place.
	 * @return Piece
	 */
	
	public static Piece getPromotionPiece(int dest, Alliance side, String code) {
		if (code.equals("Q")) {
			return new Queen(dest, side, false);
		} else if (code.equals("R")) {
			return new Rook(dest, side, false);
		} else if (code.equals("N")) {
			return new Knight(dest, side, false);
		} else if (code.equals("B")) {
			return new Bishop(dest, side, false);
		} else {
			throw new java.lang.RuntimeException();
		}
		
	}

}