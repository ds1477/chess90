package pieces;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import board.Board;
import board.BoardMethods;
import board.Move;
import board.Square;
import board.Alliance;

/**
 * @author Daniel Samojlik & Elaine Yang
 *         <p>
 *         This Bishop class extends the Piece class and will be used to
 *         calculate the legal moves of the bishop piece.
 */

public class Bishop extends Piece {

	/**
	 * Here is the constructor for the first bishop piece.
	 * 
	 * @param piecePosition
	 * @param pieceAlliance
	 */

	public Bishop(final int piecePosition, final Alliance pieceAlliance) {
		super(PieceType.BISHOP, piecePosition, pieceAlliance, true);
	}
	
	/**
	 * Here is the constructor for a bishop piece with an isFirstMove parameter.
	 * @param piecePosition
	 * @param pieceAlliance
	 * @param isFirstMove
	 */
	public Bishop(final int piecePosition, final Alliance pieceAlliance, final boolean isFirstMove) {
		super(PieceType.BISHOP, piecePosition, pieceAlliance, isFirstMove);
	}

	/**
	 * This variable contains the possible coordinate vectors that the bishop can
	 * move. This was derived from having a bishop is the center square and
	 * observing that the bishop moves along the two diagonals in multiples of 7 and
	 * 9.
	 */

	private static int[] Candidate_Move_Offset = { -9, -7, 7, 9 };

	/**
	 * Here is the abstract method implementation for legal moves of the bishop.
	 * 
	 * @param board
	 * @return ArrayList of legal moves
	 */

	@Override
	public List<Move> calculateLegalMoves(final Board board) {

		/**
		 * Here is a variable of type List will be used to store all the legal moves
		 * that can be made.
		 */

		final List<Move> legalMoves = new ArrayList<>();

		/**
		 * This for loop cycles through the possible offsets and determines if the
		 * possible coordinate is valid. The edge cases are checked for where the offsets do not work.
		 */

		for (final int offsetCandidate : Candidate_Move_Offset) {
			int possibleCoordinate = this.piecePosition;

			while (BoardMethods.doesCoordinateExist(possibleCoordinate)) {

				if (firstColumnEdge(possibleCoordinate, offsetCandidate)
						|| eighthColumnEdge(possibleCoordinate, offsetCandidate)) {
					break;
				}

				possibleCoordinate += offsetCandidate;

				if (BoardMethods.doesCoordinateExist(possibleCoordinate)) {

					final Square candidateDestinationSquare = board.getSquare(possibleCoordinate);

					if (!candidateDestinationSquare.isSquareOccupied()) {
						legalMoves.add(new Move.NonAttackingMove(board, this, possibleCoordinate));
					}

					else {
						final Piece pieceAtDestination = candidateDestinationSquare.getPiece();
						final Alliance pieceAlliance = pieceAtDestination.getPieceAlliance();

						if (this.pieceAlliance != pieceAlliance) {
							legalMoves.add(new Move.AttackingMove(board, this, possibleCoordinate, pieceAtDestination));
						}
						break;
					}
				}
			}
		}
		return Collections.unmodifiableList(legalMoves);
	}
	
	/**
	 * This method prints the String representation of the piece onto the board. 
	 * @return String
	 */
	
	@Override
	public String toString() {
		if (this.pieceAlliance.isWhite()) {
			return "w" + PieceType.BISHOP.toString();
		}
		else {
			return "b" + PieceType.BISHOP.toString();
		}
	}
	
	@Override
	public Piece movePiece(Move move) {
		return new Bishop(move.getDestinationCoordinate(), move.getMovedPiece().getPieceAlliance(), false);
	}

	/**
	 * This method defines an edge case where the above logic breaks down for
	 * the given offset values. If the current position of the bishop resides in the
	 * given column, then the respective offsets would lead to invalid
	 * moves.
	 * 
	 * @param currentPosition	position of piece 
	 * @param offsetCandidate	offset to a new possible position
	 * @return boolean array	boolean array corresponding to a current position and offset value 
	 */

	private static boolean firstColumnEdge(final int currentPosition, final int offsetCandidate) {
		return BoardMethods.First_Column[currentPosition] && (offsetCandidate == -9 || offsetCandidate == 7);
	}
	
	/**
	 * This method defines an edge case where the above logic breaks down for
	 * the given offset values. If the current position of the bishop resides in the
	 * given column, then the respective offsets would lead to invalid
	 * moves.
	 * 
	 * @param currentPosition	position of piece 
	 * @param offsetCandidate	offset to a new possible position
	 * @return boolean array	boolean array corresponding to a current position and offset value 
	 */

	private static boolean eighthColumnEdge(final int currentPosition, final int offsetCandidate) {
		return BoardMethods.Eighth_Column[currentPosition] && (offsetCandidate == -7 || offsetCandidate == 9);
	}
}
