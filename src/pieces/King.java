package pieces;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import board.Alliance;
import board.Board;
import board.BoardMethods;
import board.Move;
import board.Square;

/**
 * @author Daniel Samojlik & Elaine Yang
 *         <p>
 *         This King class extends the Piece class and will be used to calculate
 *         the legal moves of the king piece.
 */

public class King extends Piece {
	
	private final boolean isCastled;
    private final boolean kingSideCastle;
    private final boolean queenSideCastle;
    
	/**
	 * Here is the constructor for the initial king piece.
	 * 
	 * @param piecePosition
	 * @param pieceAlliance
	 */
	public King(final int piecePosition, final Alliance pieceAlliance, final boolean kingSideCastle, final boolean queenSideCastle) {
		super(PieceType.KING, piecePosition, pieceAlliance, true);
		this.isCastled = false;
		this.kingSideCastle = kingSideCastle;
		this.queenSideCastle = queenSideCastle;
	}
	
	/**
	 * Here is the constructor for subsequent king piece instantiations.
	 * @param piecePosition
	 * @param pieceAlliance
	 * @param isFirstMove
	 * @param isCastled
	 * @param kingSideCastle
	 * @param queenSideCastle
	 */
	
	public King(final int piecePosition, final Alliance pieceAlliance, final boolean isFirstMove, final boolean isCastled, final boolean kingSideCastle, final boolean queenSideCastle) {
		super(PieceType.KING, piecePosition, pieceAlliance, isFirstMove);
		this.isCastled = isCastled;
		this.kingSideCastle = kingSideCastle;
		this.queenSideCastle = queenSideCastle;
	}
	
	/**
	 * This method checks if king is castled.
	 * @return boolean
	 */
	
	public boolean isCastled() {
			return this.isCastled;
	}
	
	/**
	 * This method checks if a king side castle is possible.
	 * @return boolean
	 */
	
	public boolean canKingSideCastle() {
		return this.kingSideCastle;
	}
	
	/**
	 * This method checks if a queen side castle is possible.
	 * @return boolean
	 */
	
	public boolean canQueenSideCastle() {
		return this.queenSideCastle;
	}
	/**
	 * This variable contains the possible coordinate offsets that the king can
	 * move. This was derived from the fact that king can only one space in any
	 * direction, given that it does not put the piece in check.
	 */

	private static int[] Candidate_Move_Offset = { -9, -8, -7, -1, 1, 7, 8, 9 };

	/**
	 * Here is the abstract method implementation for legal moves of the pawn.
	 * 
	 * @param board
	 * @return ArrayList
	 */

	@Override
	public List<Move> calculateLegalMoves(Board board) {

		/**
		 * An array list will be used to store all the legal moves that can be made.
		 */

		final List<Move> legalMoves = new ArrayList<>();

		/**
		 * This for loop cycles through the possible offsets and determines if the
		 * possible coordinate is valid. The logic is analogous to that of the knight
		 * piece. The edge cases are checked for where the offsets do not work.
		 */
		
		for (final int offsetCandidate : Candidate_Move_Offset) {
			final int possibleCoordinate = this.piecePosition + offsetCandidate;

			if (firstColumnEdge(this.piecePosition, offsetCandidate)
					|| eighthColumnEdge(this.piecePosition, offsetCandidate)) {
				continue;
			}

			if (BoardMethods.doesCoordinateExist(possibleCoordinate)) {
				final Square candidateDestinationSquare = board.getSquare(possibleCoordinate);

				if (!candidateDestinationSquare.isSquareOccupied()) {
					legalMoves.add(new Move.NonAttackingMove(board, this, possibleCoordinate));
				}

				else {
					final Piece pieceAtDestination = candidateDestinationSquare.getPiece();
					final Alliance pieceAlliance = pieceAtDestination.getPieceAlliance();

					if (this.pieceAlliance != pieceAlliance) {
						legalMoves.add(new Move.AttackingMove(board, this, possibleCoordinate, pieceAtDestination));
					}
				}
			}
		}
		if (this.isFirstMove) {
			if (this.getPieceAlliance() == Alliance.WHITE) {
				if (board.getSquare(63).isSquareOccupied()) {
					Piece maybeRook1 = board.getSquare(63).getPiece();
					if (maybeRook1.pieceType.equals(PieceType.ROOK)) {
						if (!board.getSquare(61).isSquareOccupied() && !board.getSquare(62).isSquareOccupied()) {
							Collection<Piece> pieces = board.getBlackPieces();
							Collection<Move> moves = new ArrayList<Move>();
							for (Piece p : pieces) {
								moves.addAll(p.calculateLegalMoves(board));
							}
							boolean canCastle = true;
							for (Move mv : moves) {
								if (mv.getDestinationCoordinate() == 61 || mv.getDestinationCoordinate() == 62) {
									canCastle = false;
								}
							}
							if (canCastle) {
								Piece king = this;
								legalMoves.add(new Move.KingSideCastleMove(board, king, 62, (Rook) maybeRook1, 63, 61));
							}
						}
					}
				}
				if (board.getSquare(56).isSquareOccupied()) {
					Piece maybeRook2 = board.getSquare(56).getPiece();
					if (maybeRook2.pieceType.equals(PieceType.ROOK)) {
						if (!board.getSquare(59).isSquareOccupied() && !board.getSquare(58).isSquareOccupied()) {
							Collection<Piece> pieces = board.getBlackPieces();
							Collection<Move> moves = new ArrayList<Move>();
							for (Piece p : pieces) {
								moves.addAll(p.calculateLegalMoves(board));
							}
							boolean canCastle = true;
							for (Move mv : moves) {
								if (mv.getDestinationCoordinate() == 59 || mv.getDestinationCoordinate() == 58) {
									canCastle = false;
								}
							}
							if (canCastle) {
								Piece king = this;
								legalMoves.add(new Move.KingSideCastleMove(board, king, 58, (Rook) maybeRook2, 56, 59));
							}
						}
					}
				}
			} else {
				if (board.getSquare(0).isSquareOccupied()) {
					Piece maybeRook1 = board.getSquare(0).getPiece();
					if (maybeRook1.pieceType.equals(PieceType.ROOK)) {
						if (!board.getSquare(3).isSquareOccupied() && !board.getSquare(2).isSquareOccupied()) {
							Collection<Piece> pieces = board.getWhitePieces();
							Collection<Move> moves = new ArrayList<Move>();
							for (Piece p : pieces) {
								moves.addAll(p.calculateLegalMoves(board));
							}
							boolean canCastle = true;
							for (Move mv : moves) {
								if (mv.getDestinationCoordinate() == 3 || mv.getDestinationCoordinate() == 2) {
									canCastle = false;
								}
							}
							if (canCastle) {
								Piece king = this;
								legalMoves.add(new Move.KingSideCastleMove(board, king, 2, (Rook) maybeRook1, 0, 3));
							}
						}
					}
				}
				if (board.getSquare(8).isSquareOccupied()) {
					Piece maybeRook2 = board.getSquare(8).getPiece();
					if (maybeRook2.pieceType.equals(PieceType.ROOK)) {
						if (!board.getSquare(5).isSquareOccupied() && !board.getSquare(6).isSquareOccupied()) {
							Collection<Piece> pieces = board.getWhitePieces();
							Collection<Move> moves = new ArrayList<Move>();
							for (Piece p : pieces) {
								moves.addAll(p.calculateLegalMoves(board));
							}
							boolean canCastle = true;
							for (Move mv : moves) {
								if (mv.getDestinationCoordinate() == 5 || mv.getDestinationCoordinate() == 6) {
									canCastle = false;
								}
							}
							if (canCastle) {
								Piece king = this;
								legalMoves.add(new Move.KingSideCastleMove(board, king, 6, (Rook) maybeRook2, 8, 5));
							}
						}
					}
				}
			}
		}
		return Collections.unmodifiableList(legalMoves);
	}
	
	/**
	 * This method prints the String representation of the piece onto the board. 
	 * @return String
	 */
	
	@Override
	public String toString() {
		if (this.pieceAlliance.isWhite()) {
			return "w" + PieceType.KING.toString();
		}
		else {
			return "b" + PieceType.KING.toString();
		}
	}

	@Override
	public Piece movePiece(Move move) {
		return new King(move.getDestinationCoordinate(), move.getMovedPiece().getPieceAlliance(), false, move.isCastlingMove(), false, false);
	}
	
	@Override
	public boolean equals(final Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof King)) {
			return false;
		}
		if (!super.equals(other)) {
			return false;
		}
		final King king = (King) other;
		return isCastled == king.isCastled;
	}

	@Override
	public int hashCode() {
		return (31 * super.hashCode()) + (isCastled ? 1 : 0);
	}
	
	/**
	 * This method defines an edge case where the above logic breaks down for the
	 * given offset values. If the current position of the king resides in the given
	 * column, then the respective offsets would lead to invalid moves.
	 * 
	 * @param currentPosition position of piece
	 * @param offsetCandidate offset to a new possible position
	 * @return boolean array boolean array corresponding to a current position and
	 *         offset value
	 */

	private static boolean firstColumnEdge(final int currentPosition, final int offsetCandidate) {
		return BoardMethods.First_Column[currentPosition]
				&& ((offsetCandidate == -9) || (offsetCandidate == -1) || (offsetCandidate == 7));
	}

	/**
	 * This method defines an edge case where the above logic breaks down for the
	 * given offset values. If the current position of the king resides in the given
	 * column, then the respective offsets would lead to invalid moves.
	 * 
	 * @param currentPosition position of piece
	 * @param offsetCandidate offset to a new possible position
	 * @return boolean array boolean array corresponding to a current position and
	 *         offset value
	 */

	private static boolean eighthColumnEdge(final int currentPosition, final int offsetCandidate) {
		return BoardMethods.Eighth_Column[currentPosition]
				&& ((offsetCandidate == -7) || (offsetCandidate == 1) || (offsetCandidate == 9));
	}

	
}