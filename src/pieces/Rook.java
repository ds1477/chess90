package pieces;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import board.Board;
import board.BoardMethods;
import board.Move;
import board.Square;
import board.Alliance;

/**
 * @author Daniel Samojlik & Elaine Yang
 *         <p>
 *         This Rook class extends the Piece class and will be used to calculate
 *         the legal moves of the rook piece. This class follows the same logic
 *         as the bishop class butt differs in the edge cases.
 */

public class Rook extends Piece {
	
	/**
	 * Here is the constructor for an initial rook piece.
	 * @param piecePosition
	 * @param pieceAlliance
	 */
	public Rook(final int piecePosition, final Alliance pieceAlliance) {
		super(PieceType.ROOK, piecePosition, pieceAlliance, true);
		
	}
	
	/**
	 * Here is the constructor for subsequent rook pieces.
	 * @param piecePosition
	 * @param pieceAlliance
	 * @param isFirstMove
	 */
	
	public Rook(final int piecePosition, final Alliance pieceAlliance, final boolean isFirstMove) {
		super(PieceType.ROOK, piecePosition, pieceAlliance, isFirstMove);
	}

	/**
	 * This variable contains the possible coordinate vectors that the rook can
	 * move. This was derived from having a rook is the center square and observing
	 * that the rook moves in the vertical and horizontal directions via multiples
	 * of 1 and 8.
	 */

	private static int[] Candidate_Move_Offset = { -8, -1, 1, 8 };

	/**
	 * Here is the abstract method implementation for legal moves of the rook.
	 * 
	 * @param board
	 * @return ArrayList of legal moves
	 */

	@Override
	public List<Move> calculateLegalMoves(final Board board) {

		/**
		 * An array list will be used to store all the legal moves
		 * that can be made.
		 */

		final List<Move> legalMoves = new ArrayList<>();

		/**
		 * This set of code determines the difference possible moves for the rook and adds the valid moves respectively.
		 */

		for (final int offsetCandidate : Candidate_Move_Offset) {
			int possibleCoordinate = this.piecePosition;

			while (BoardMethods.doesCoordinateExist(possibleCoordinate)) {

				if (firstColumnEdge(possibleCoordinate, offsetCandidate)
						|| eighthColumnEdge(possibleCoordinate, offsetCandidate)) {
					break;
				}

				possibleCoordinate += offsetCandidate;

				if (BoardMethods.doesCoordinateExist(possibleCoordinate)) {

					final Square candidateDestinationSquare = board.getSquare(possibleCoordinate);

					if (!candidateDestinationSquare.isSquareOccupied()) {
						legalMoves.add(new Move.NonAttackingMove(board, this, possibleCoordinate));
					}

					else {
						final Piece pieceAtDestination = candidateDestinationSquare.getPiece();
						final Alliance pieceAlliance = pieceAtDestination.getPieceAlliance();

						if (this.pieceAlliance != pieceAlliance) {
							legalMoves.add(new Move.AttackingMove(board, this, possibleCoordinate, pieceAtDestination));
						}
						break;
					}
				}
			}
		}
		return Collections.unmodifiableList(legalMoves);
	}
	
	/**
	 * This method prints the String representation of the piece onto the board. 
	 * @return String
	 */
	
	@Override
	public String toString() {
		if (this.pieceAlliance.isWhite()) {
			return "w" + PieceType.ROOK.toString();
		}
		else {
			return "b" + PieceType.ROOK.toString();
		}
	}
	
	@Override
	public Piece movePiece(Move move) {
		return new Rook(move.getDestinationCoordinate(), move.getMovedPiece().getPieceAlliance(), false);
	}

	/**
	 * This method defines an edge case where the above logic breaks down for
	 * the given offset values. If the current position of the rook resides in the
	 * given column, then the respective offsets would lead to invalid
	 * moves.
	 * 
	 * @param currentPosition	position of piece 
	 * @param offsetCandidate	offset to a new possible position
	 * @return boolean array	boolean array corresponding to a current position and offset value 
	 */

	private static boolean firstColumnEdge(final int currentPosition, final int offsetCandidate) {
		return BoardMethods.First_Column[currentPosition] && (offsetCandidate == -1);
	}
	
	/**
	 * This method defines an edge case where the above logic breaks down for
	 * the given offset values. If the current position of the rook resides in the
	 * given column, then the respective offsets would lead to invalid
	 * moves.
	 * 
	 * @param currentPosition	position of piece 
	 * @param offsetCandidate	offset to a new possible position
	 * @return boolean array	boolean array corresponding to a current position and offset value 
	 */

	private static boolean eighthColumnEdge(final int currentPosition, final int offsetCandidate) {
		return BoardMethods.Eighth_Column[currentPosition] && (offsetCandidate == 1);
	}
}