package pieces;

import java.util.List;

import board.Board;
import board.Move;
import board.Alliance;

/**
 * @author Daniel Samojlik and Elaine Yang
 *         <p>
 *         This Piece class is an abstract class and contains one abstract
 *         method, calculateLegalMoves.
 */

public abstract class Piece {

	/**
	 * This enum declares the position of the piece.
	 */
	protected final PieceType pieceType;

	/**
	 * This variable declares the position of the piece.
	 */
	protected final int piecePosition;

	/**
	 * This enum declares the color of the piece.
	 */
	protected final Alliance pieceAlliance;

	/**
	 * This variable declares whether the piece is making its first move.
	 */

	protected final boolean isFirstMove;
	
	/**
	 * This variable is used to keep track if a pawn is in an enpassant state. 
	 */
	
	public boolean empass;
	
	/**
	 * This field holds the hash code of a piece.
	 */
	private final int cachedHashCode;

	/**
	 * Here is a constructor for a Piece.
	 * 
	 * @param pieceType
	 * @param piecePosition
	 * @param pieceAlliance
	 * @param isFirstMove
	 */

	Piece(final PieceType pieceType, int piecePosition, final Alliance pieceAlliance, final boolean isFirstMove) {
		this.pieceType = pieceType;
		this.piecePosition = piecePosition;
		this.pieceAlliance = pieceAlliance;
		this.isFirstMove = isFirstMove;
		this.cachedHashCode = computeHashCode();
		this.empass = false;
	}
	
	/**
	 * This method computes the hash code for a given piece object.
	 * @return int
	 */

	private int computeHashCode() {
		int result = pieceType.hashCode();
		result = 31 * result + pieceAlliance.hashCode();
		result = 31 * result + piecePosition;
		result = 31 * result + (isFirstMove ? 1 : 0);
		return result;
	}

	/**
	 * This equals method is used when comparing the pieces in a collection since object equality is wanted over reference equality.
	 * 
	 * @return boolean
	 */
	@Override
	public boolean equals(final Object other) {
		if (this == other) {
			return true;
		}

		if (!(other instanceof Piece)) {
			return false;
		}

		final Piece otherPiece = (Piece) other;
		return pieceType == otherPiece.getPieceType() && piecePosition == otherPiece.getPiecePosition()
				&& pieceAlliance == otherPiece.getPieceAlliance() && isFirstMove == otherPiece.isFirstMove();
	}

	/**
	 * This hash code needs to be overridden given that the equals method was overridden.
	 * 
	 * @return int
	 */
	@Override
	public int hashCode() {
		return this.cachedHashCode;
	}

	/**
	 * This is a method to return the piece's piece type.
	 * 
	 * @return pieceType
	 */
	public PieceType getPieceType() {
		return this.pieceType;
	}

	/**
	 * This is a method to return the piece's position.
	 * 
	 * @return pieceType
	 */

	public int getPiecePosition() {
		return this.piecePosition;
	}

	/**
	 * This is a method to return the piece's color.
	 * 
	 * @return Alliance
	 */

	public Alliance getPieceAlliance() {
		return this.pieceAlliance;
	}

	/**
	 * This is a method to return the piece's position.
	 * 
	 * @return pieceType
	 */

	public boolean isFirstMove() {
		return this.isFirstMove;
	}

	/**
	 * This abstract method will be responsible for calculating the legal moves of a
	 * piece. All of the pieces will have their own behavior for calculating their
	 * legal moves.
	 */

	public abstract List<Move> calculateLegalMoves(final Board board);

	/**
	 * This abstract method will take a move and apply it to the existing immutable
	 * piece, and return a new piece, which is the same but with an updated piece
	 * position.
	 * 
	 * @param move
	 * @return Piece piece with new piece position
	 */
	public abstract Piece movePiece(Move move);

	/**
	 * PieceType enum is used to designate strings to the pieces and uses the
	 * toString method to assign the given enum to them.
	 */
	public enum PieceType {

		PAWN("p") {
			@Override
			public boolean isKing() {
				return false;
			}

			@Override
			public boolean isRook() {
				return false;
			}
		},
		ROOK("R") {
			@Override
			public boolean isKing() {
				return false;
			}

			@Override
			public boolean isRook() {
				return true;
			}
		},
		KNIGHT("N") {
			@Override
			public boolean isKing() {
				return false;
			}

			@Override
			public boolean isRook() {
				return false;
			}
		},
		BISHOP("B") {
			@Override
			public boolean isKing() {
				return false;
			}

			@Override
			public boolean isRook() {
				return false;
			}
		},
		QUEEN("Q") {
			@Override
			public boolean isKing() {
				return false;
			}

			@Override
			public boolean isRook() {
				return false;
			}
		},
		KING("K") {
			@Override
			public boolean isKing() {
				return true;
			}

			@Override
			public boolean isRook() {
				return false;
			}
		};

		/**
		 * This field defines the string representation of a piece.
		 */
		private String pieceName;

		/**
		 * Here is the constructor for a piece type.
		 * 
		 * @param pieceName
		 */
		PieceType(final String pieceName) {
			this.pieceName = pieceName;
		}

		/**
		 * A method to print the piece type string.
		 * 
		 * @return String
		 */
		@Override
		public String toString() {
			return this.pieceName;
		}

		/**
		 * This abstract method will look at each piece type and determine if it is a
		 * king piece. It will return true only for a king piece.
		 * 
		 * @return boolean
		 */
		public abstract boolean isKing();

		/**
		 * This abstract method will look at each piece type and determine if it is a
		 * rook piece. It will return true only for a rook piece.
		 * 
		 * @return boolean
		 */
		public abstract boolean isRook();
	}
}
