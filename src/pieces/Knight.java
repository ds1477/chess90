package pieces;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import board.Board;
import board.BoardMethods;
import board.Move;
import board.Square;
import board.Alliance;

/**
 * @author Daniel Samojlik & Elaine Yang
 *         <p>
 *         This Knight class extends the Piece class and will be used to
 *         calculate the legal moves of the knight piece.
 *
 */

public class Knight extends Piece {

	/**
	 * Here is the constructor for the initial knight piece.
	 * 
	 * @param piecePosition
	 * @param pieceAlliance
	 */
	public Knight(final int piecePosition, final Alliance pieceAlliance) {
		super(PieceType.KNIGHT, piecePosition, pieceAlliance, true);
	}
	
	/**
	 * Here is the constructor for subsequent knight pieces.
	 * @param piecePosition
	 * @param pieceAlliance
	 * @param isFirstMove
	 */
	public Knight(final int piecePosition, final Alliance pieceAlliance, final boolean isFirstMove) {
		super(PieceType.KNIGHT, piecePosition, pieceAlliance, isFirstMove);
	}
	
	/**
	 * The variable holds the possible candidate moves of the Knight. This was
	 * derived from seeing that when a knight is in the center of the board, the max
	 * amount of possible candidate moves is eight. And possible moves by the knight
	 * is offered from these offset integers.
	 */
	private final static int[] Candidate_Move_Offset = { -17, -15, -10, -6, 6, 10, 15, 17 };

	

	/**
	 * Here is the abstract method implementation for legal moves of the knight.
	 * 
	 * @param board
	 * @return ArrayList of legal moves
	 */
	@Override
	public List<Move> calculateLegalMoves(final Board board) {

		/**
		 * An array list will be used to store all the legal moves
		 * that can be made.
		 */
		final List<Move> legalMoves = new ArrayList<>();

		/**
		 * This for loop cycles through the possible offsets and determines if the
		 * possible coordinate is valid. The edge cases are checked for where the offsets do not work.
		 */

		for (final int offsetCandidate : Candidate_Move_Offset) {
			int possibleCoordinate = this.piecePosition + offsetCandidate;

			if (BoardMethods.doesCoordinateExist(possibleCoordinate)) {

				if (firstColumnEdge(this.piecePosition, offsetCandidate)
						|| secondColumnEdge(this.piecePosition, offsetCandidate)
						|| seventhColumnEdge(this.piecePosition, offsetCandidate)
						|| eighthColumnEdge(this.piecePosition, offsetCandidate)) {
					continue;
				}

				final Square candidateDestinationSquare = board.getSquare(possibleCoordinate);

				if (!candidateDestinationSquare.isSquareOccupied()) {
					legalMoves.add(new Move.NonAttackingMove(board, this, possibleCoordinate));
				}

				else {
					final Piece pieceAtDestination = candidateDestinationSquare.getPiece();
					final Alliance pieceAlliance = pieceAtDestination.getPieceAlliance();

					if (this.pieceAlliance != pieceAlliance) {
						legalMoves.add(new Move.AttackingMove(board, this, possibleCoordinate, pieceAtDestination));
					}
				}

			}
		}

		return Collections.unmodifiableList(legalMoves);
	}
	
	/**
	 * This method prints the String representation of the piece onto the board. 
	 * @return String
	 */
	
	@Override
	public String toString() {
		if (this.pieceAlliance.isWhite()) {
			return "w" + PieceType.KNIGHT.toString();
		}
		else {
			return "b" + PieceType.KNIGHT.toString();
		}
	}
	
	@Override
	public Piece movePiece(Move move) {
		return new Knight(move.getDestinationCoordinate(), move.getMovedPiece().getPieceAlliance(), false);
	}

	/**
	 * This method defines an edge case where the above logic breaks down for
	 * the given offset values. If the current position of the knight resides in the
	 * given column, then the respective offsets would lead to invalid
	 * moves.
	 * 
	 * @param currentPosition	position of piece 
	 * @param offsetCandidate	offset to a new possible position
	 * @return boolean array	boolean array corresponding to a current position and offset value 
	 */

	private static boolean firstColumnEdge(final int currentPosition, final int offsetCandidate) {
		return BoardMethods.First_Column[currentPosition] && ((offsetCandidate == -17) || (offsetCandidate == -10)
				|| (offsetCandidate == 6) || (offsetCandidate == 15));
	}
	
	/**
	 * This method defines an edge case where the above logic breaks down for
	 * the given offset values. If the current position of the knight resides in the
	 * given column, then the respective offsets would lead to invalid
	 * moves.
	 * 
	 * @param currentPosition	position of piece 
	 * @param offsetCandidate	offset to a new possible position
	 * @return boolean array	boolean array corresponding to a current position and offset value 
	 */

	private static boolean secondColumnEdge(final int currentPosition, final int offsetCandidate) {
		return BoardMethods.Second_Column[currentPosition] && ((offsetCandidate == -10) || (offsetCandidate == 6));
	}
	
	/**
	 * This method defines an edge case where the above logic breaks down for
	 * the given offset values. If the current position of the knight resides in the
	 * given column, then the respective offsets would lead to invalid
	 * moves.
	 * 
	 * @param currentPosition	position of piece 
	 * @param offsetCandidate	offset to a new possible position
	 * @return boolean array	boolean array corresponding to a current position and offset value 
	 */

	private static boolean seventhColumnEdge(final int currentPosition, final int offsetCandidate) {
		return BoardMethods.Seventh_Column[currentPosition] && ((offsetCandidate == -6) || (offsetCandidate == 10));
	}
	
	/**
	 * This method defines an edge case where the above logic breaks down for
	 * the given offset values. If the current position of the knight resides in the
	 * given column, then the respective offsets would lead to invalid
	 * moves.
	 * 
	 * @param currentPosition	position of piece 
	 * @param offsetCandidate	offset to a new possible position
	 * @return boolean array	boolean array corresponding to a current position and offset value 
	 */

	private static boolean eighthColumnEdge(final int currentPosition, final int offsetCandidate) {
		return BoardMethods.Eighth_Column[currentPosition] && ((offsetCandidate == -15) || (offsetCandidate == -6)
				|| (offsetCandidate == 10) || (offsetCandidate == 17));
	}
}
